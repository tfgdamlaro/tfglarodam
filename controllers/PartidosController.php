<?php

namespace app\controllers;

use app\models\Partidos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Categoria;
use yii\helpers\ArrayHelper;
use app\models\PartidosSearch;
use yii;

/**
 * PartidosController implements the CRUD actions for Partidos model.
 */
class PartidosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Partidos models.
     *
     * @return string
     */
    public function actionIndex()
    {
         $searchModel = new PartidosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partidos model.
     * @param int $id_partidos Id Partidos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_partidos)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_partidos),
        ]);
    }

    /**
     * Creates a new Partidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Partidos();
        $data = ArrayHelper::map(Categoria::find()->all(), 'cod_categoria', 'cod_categoria');
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_partidos' => $model->id_partidos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]); 
    }

    /**
     * Updates an existing Partidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_partidos Id Partidos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_partidos)
    {
        $model = $this->findModel($id_partidos);
        $data = ArrayHelper::map(Categoria::find()->all(), 'cod_categoria', 'cod_categoria');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_partidos' => $model->id_partidos]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Partidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_partidos Id Partidos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_partidos)
    {
        $this->findModel($id_partidos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_partidos Id Partidos
     * @return Partidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_partidos)
    {
        if (($model = Partidos::findOne(['id_partidos' => $id_partidos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
