<?php

namespace app\controllers;

use app\models\Juegan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Jugadores;
use yii\helpers\ArrayHelper;
use app\models\Partidos;
use Yii;
use app\models\JueganSearch;



/**
 * JueganController implements the CRUD actions for Juegan model.
 */
class JueganController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Juegan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new JueganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Juegan model.
     * @param int $id_juegan Id Juegan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_juegan)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_juegan),
        ]);
    }

    /**
     * Creates a new Juegan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Juegan();
        $data = ArrayHelper::map(Jugadores::find()->all(), 'dni', 'dni');
        $data1 = ArrayHelper::map(Partidos::find()->all(), 'id_partidos', 'id_partidos');
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_juegan' => $model->id_juegan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
            'data1' => $data1,
        ]);
    }

    /**
     * Updates an existing Juegan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_juegan Id Juegan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_juegan)
    {
        $model = $this->findModel($id_juegan);
        $data = ArrayHelper::map(Jugadores::find()->all(), 'dni', 'dni');
        $data1 = ArrayHelper::map(Partidos::find()->all(), 'id_partidos', 'id_partidos');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_juegan' => $model->id_juegan]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
            'data1' => $data1,
        ]);
    }

    /**
     * Deletes an existing Juegan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_juegan Id Juegan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_juegan)
    {
        $this->findModel($id_juegan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Juegan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_juegan Id Juegan
     * @return Juegan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_juegan)
    {
        if (($model = Juegan::findOne(['id_juegan' => $id_juegan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionInsertarConvocatorias(){
        $model = new Juegan();
        $id_partidos = Yii::$app->request->post('Juegan')['id_partidos'];
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // Borra los registros existentes para el ID de partido actual
            Juegan::deleteAll(['id_partidos' => $id_partidos]);

            $jugadoresSeleccionados = Yii::$app->request->post('jugadoresSeleccionados');

            if (!empty($jugadoresSeleccionados)) {
                foreach ($jugadoresSeleccionados as $dni) {
                    $nuevaConvocatoria = new Juegan();
                    $nuevaConvocatoria->id_partidos = $id_partidos;
                    $nuevaConvocatoria->dni = $dni;
                    // Asigna otros atributos del modelo según sea necesario
                    $nuevaConvocatoria->save();
                }
                Yii::$app->session->setFlash('success', 'Convocatoria generada exitosamente.');
                return $this->redirect(['/juegan/convocados', 'codCategoria' => Yii::$app->request->post('codCategoria'), 'id_partidos' => $model->id_partidos]);
            } else {
                Yii::$app->session->setFlash('error', 'Debe seleccionar al menos un jugador.');
                return $this->redirect(['juegan/generarconvocatorias', 'codCategoria' => Yii::$app->request->post('codCategoria'), 'id_partidos' => $model->id_partidos]);
               
            }
        }
        return $this->render('insertar-convocatorias', [
            'model' => $model,
            'id_partidos' => $id_partidos,
        ]);
    }

    public function actionGuardarActualizacion(){
        $model = new Juegan();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // Eliminar jugadores seleccionados existentes
            Juegan::deleteAll(['id_partidos' => $model->id_partidos]);

            // Guardar jugadores seleccionados actualizados
            foreach ($model->jugadoresSeleccionados as $dni) {
                $nuevaConvocatoria = new Juegan();
                $nuevaConvocatoria->id_partidos = $model->id_partidos;
                $nuevaConvocatoria->dni = $dni;
                $nuevaConvocatoria->save();
            }

            return $this->redirect(['/juegan/convocados', 'codCategoria' => Yii::$app->request->post('codCategoria'), 'id_partidos' => $model->id_partidos]);

        }

        // Si la validación o el guardado fallan, vuelve a mostrar la vista de actualización
        return $this->render('actualizar', ['model' => $model]);
    }
    
    public function actionGenerarconvocatorias($codCategoria,$id_partidos){
        $jugadores = new ActiveDataProvider([
            'query' => Jugadores::find()->select("dni, nombre, apellidos, dorsal")->where("cod_categoria = :codCategoria", [':codCategoria' => $codCategoria]),
        ]);
        $convocatoria = Juegan::findOne(['id_partidos' => $id_partidos]);
        if($convocatoria){
             Yii::$app->session->setFlash('error', 'Ya hay una convocatoria creada.');
             return $this->redirect(['/juegan/convocados', 'codCategoria' => $codCategoria, 'id_partidos' => $id_partidos]);
        }else{
            return $this->render("create-convocatoria",[
                "jugadores" => $jugadores,
                "campos" => ['dni, nombre','apellidos','dorsal'],
                "id_partidos" => $id_partidos,
                "codCategoria" => $codCategoria
            ]);
        }
        
    }
    public function actionActualizarconvocatorias($codCategoria,$id_partidos){
        $convocatoria = Juegan::findOne(['id_partidos' => $id_partidos]);

        if (!$convocatoria) {
            Yii::$app->session->setFlash('error', 'Primero debes crear una convocatoria.');
            return $this->redirect(['/juegan/convocados', 'codCategoria' => $codCategoria, 'id_partidos' => $id_partidos]);
        }else{
            $jugadores = new ActiveDataProvider([
                'query' => Jugadores::find()->select("dni ,nombre, apellidos, dorsal")->where("cod_categoria = '$codCategoria'"),
            ]);

            return $this->render("update-convocatoria", [
                "jugadores" => $jugadores,
                "campos" => ['dni, nombre', 'apellidos', 'dorsal'],
                "id_partidos" => $id_partidos,
                "codCategoria" => $codCategoria
            ]);
        }    
    }
    public function actionEliminarconvocatoria($id_partidos, $codCategoria)
    {
        $convocatoria = Juegan::findOne(['id_partidos' => $id_partidos]);

        if (!$convocatoria) {
            Yii::$app->session->setFlash('error', 'No hay convocatoria la convocatoria.');
        } else {
            Juegan::deleteAll(['id_partidos' => $id_partidos]);

            Yii::$app->session->setFlash('success', 'Convocatoria eliminada exitosamente.');
        }

        return $this->redirect(['/juegan/convocados', 'codCategoria' => $id_partidos, 'id_partidos' => $codCategoria]);
    }
    
    public function actionConvocados($codCategoria,$id_partidos){
        
        $convocados = Jugadores::find()
            ->select(['dorsal', 'nombre', 'apellidos','id_partidos'])
            ->join('INNER JOIN', 'juegan', 'juegan.dni = jugadores.dni')
            ->where(['id_partidos' => $id_partidos])
            ->all();
        
        if(empty($convocados) && $codCategoria == null){
            Yii::$app->session->setFlash('error', 'No hay ninguna convocatoria creada para este partido. Debes crear una');
            return $this->render('convocatoria', ['convocados' => $convocados]);
        }

        return $this->render('convocatoria', [
            'convocados' => $convocados,
            'codCategoria' => $codCategoria,
            'id_partidos' => $id_partidos
            ]);
    }











    
    


}
