<?php

namespace app\controllers;

use app\models\Jugadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Categoria;
use app\models\JugadoresSearch;
use yii;

/**
 * JugadoresController implements the CRUD actions for Jugadores model.
 */
class JugadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Jugadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new JugadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jugadores model.
     * @param string $dni Dni
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni),
        ]);
    }

    /**
     * Creates a new Jugadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Jugadores();
        $data = ArrayHelper::map(Categoria::find()->all(), 'cod_categoria', 'cod_categoria');
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dni' => $model->dni]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing Jugadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni Dni
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni)
    {
        $model = $this->findModel($dni);
        $data = ArrayHelper::map(Categoria::find()->all(), 'cod_categoria', 'cod_categoria');
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni' => $model->dni]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }


    /**
     * Deletes an existing Jugadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni Dni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni)
    {
        $this->findModel($dni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jugadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni Dni
     * @return Jugadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni)
    {
        if (($model = Jugadores::findOne(['dni' => $dni])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
