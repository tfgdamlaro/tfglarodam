<?php

namespace app\controllers;

use app\models\Patrocinadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Directivos;
use app\models\PatrocinadoresSearch;
use yii;

/**
 * PatrocinadoresController implements the CRUD actions for Patrocinadores model.
 */
class PatrocinadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Patrocinadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PatrocinadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patrocinadores model.
     * @param int $id_patrocinadores Id Patrocinadores
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_patrocinadores)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_patrocinadores),
        ]);
    }

    /**
     * Creates a new Patrocinadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Patrocinadores();
        $data = ArrayHelper::map(Directivos::find()->all(), 'dni', 'dni');
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_patrocinadores' => $model->id_patrocinadores]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing Patrocinadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_patrocinadores Id Patrocinadores
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_patrocinadores)
    {
        $model = $this->findModel($id_patrocinadores);
        $data = ArrayHelper::map(Directivos::find()->all(), 'dni', 'dni');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_patrocinadores' => $model->id_patrocinadores]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Patrocinadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_patrocinadores Id Patrocinadores
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_patrocinadores)
    {
        $this->findModel($id_patrocinadores)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Patrocinadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_patrocinadores Id Patrocinadores
     * @return Patrocinadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_patrocinadores)
    {
        if (($model = Patrocinadores::findOne(['id_patrocinadores' => $id_patrocinadores])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
