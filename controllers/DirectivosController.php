<?php

namespace app\controllers;

use app\models\Directivos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DirectivosSearch;
use yii;


/**
 * DirectivosController implements the CRUD actions for Directivos model.
 */
class DirectivosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Directivos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DirectivosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dni' => SORT_DESC,
                ]
            ],
            */
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]); 
    }

    /**
     * Displays a single Directivos model.
     * @param string $dni Dni
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni),
        ]);
    }

    /**
     * Creates a new Directivos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Directivos();
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dni' => $model->dni]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Directivos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni Dni
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni)
    {
        $model = $this->findModel($dni);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni' => $model->dni]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Directivos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni Dni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni)
    {
        $this->findModel($dni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Directivos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni Dni
     * @return Directivos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni)
    {
        if (($model = Directivos::findOne(['dni' => $dni])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
