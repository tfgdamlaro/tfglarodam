<?php

namespace app\controllers;

use app\models\Cuotas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Jugadores;
use yii\helpers\ArrayHelper;

/**
 * CuotasController implements the CRUD actions for Cuotas model.
 */
class CuotasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Cuotas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Cuotas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_cuotas' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cuotas model.
     * @param int $id_cuotas Id Cuotas
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_cuotas)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_cuotas),
        ]);
    }

    /**
     * Creates a new Cuotas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Cuotas();
        $data = ArrayHelper::map(Jugadores::find()->all(), 'dni', 'dni');
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_cuotas' => $model->id_cuotas]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing Cuotas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_cuotas Id Cuotas
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_cuotas)
    {
        $model = $this->findModel($id_cuotas);
        $data = ArrayHelper::map(Jugadores::find()->all(), 'dni', 'dni');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_cuotas' => $model->id_cuotas]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Cuotas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_cuotas Id Cuotas
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_cuotas)
    {
        $this->findModel($id_cuotas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cuotas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_cuotas Id Cuotas
     * @return Cuotas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_cuotas)
    {
        if (($model = Cuotas::findOne(['id_cuotas' => $id_cuotas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
