<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Partidos;
use app\models\Horarioentrenamientos;
use app\models\Jugadores;
use app\models\Preparadores;
use app\models\Categoria;
use yii\data\sqlDataProvider;
use edofre\fullcalendar\models\Event;
use yii\db\Query;
use yii\helpers\Json;
use app\models\NewsForm;
use yii\web\Cookie;
use app\models\Noticias;
use yii\helpers\Html;
use app\models\PartidosSearch;



class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // Obtener la última noticia desde tu almacenamiento persistente
      $ultimaNoticia = Noticias::find()
        ->orderBy(['id' => SORT_DESC])
        ->limit(3)
        ->all();

      return $this->render('index', [
          'ultimaNoticia' => $ultimaNoticia,
      ]);
    }



    /**
     * Login action.s
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->layout = 'main-left'; // Agregar esta línea para establecer el nuevo layout
            return $this->goBack();
        }

        $model->contraseña = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionInfocategoria() {
        $codCategoriaM = yii:: $app->db->createCommand("select cod_categoria from categoria where cod_categoria LIKE '%M'")->queryAll();
        
        $codCategoriaF = yii:: $app->db->createCommand("select cod_categoria from categoria where cod_categoria LIKE '%F'")->queryAll();
        
        return $this->render('infoCategorias', [
            "codCategoriaM" => $codCategoriaM,  
            "codCategoriaF" => $codCategoriaF,
        ]);
    }
    
    public function actionDatoscategoria($codCategoria){
        $preparadores = yii:: $app->db->createCommand("select nombre, apellidos, telefono, dni from preparadores inner join categoria using(dni) where cod_categoria = '$codCategoria'")->queryAll();
        
        
        $entrenadores = yii:: $app->db->createCommand("select nombre, apellidos, telefono from entrenadores where cod_categoria = '$codCategoria'")->queryAll();
        
        $jugadores = new ActiveDataProvider([
            'query' => Jugadores::find()->select("nombre, apellidos, dorsal")->where("cod_categoria = '$codCategoria'"),
        ]);
               
        return $this->render("datoscategoria",[
            "preparadores" => $preparadores,
            "entrenadores" => $entrenadores,
            "codCategoria" => $codCategoria,
            "jugadores" => $jugadores
        ]);
    }
    
    public function actionMostrarentrenamientos()
    {
        $categorias = [
            'AL_F' => 'Alevín Femenino',
            'AL_M' => 'Alevín Masculino',
            'BN_F' => 'Benjamín Femenino',
            'BN_M' => 'Benjamín Masculino',
            'CAD_F' => 'Cadete Femenino',
            'CAD_M' => 'Cadete Masculino',
            'DH_F' => 'Division de Honor Femenino',
            'DH_M' => 'Division de Honor Masculino',
            'IF_F' => 'Infantil Femenino',
            'IF_M' => 'Infantil Masculino',
            'JUV_F' => 'Juvenil Femenino',
            'JUV_M' => 'Juvenil Masculino',
            'PB_F' => 'Prebenjamín Femenino',
            'PB_M' => 'Prebenjamín Masculino',
            'PRI_F' => 'Primera Femenino',
            'PRI_M' => 'Primera Masculino',
            'SEG_F' => 'Segunda Femenino',
            'SEG_M' => 'Segunda Masculino',
        ];
        $color = [
            'AL_F' => '#FF0000', // Alevín Femenino
            'AL_M' => '#FFCCCC', // Alevín Masculino
            'BN_F' => '#0000FF', // Benjamín Femenino
            'BN_M' => '#CCCCFF', // Benjamín Masculino
            'CAD_F' => '#00FF00', // Cadete Femenino
            'CAD_M' => '#CCFFCC', // Cadete Masculino
            'DH_F' => '#A52A2A', // División de Honor Femenino
            'DH_M' => '#D2B48C', // División de Honor Masculino
            'IF_F' => '#FFFF00', // Infantil Femenino
            'IF_M' => '#FFFFCC', // Infantil Masculino
            'JUV_F' => '#FFA500', // Juvenil Femenino
            'JUV_M' => '#FFDAB9', // Juvenil Masculino
            'PB_F' => '#00FFFF', // Prebenjamín Femenino
            'PB_M' => '#E0FFFF', // Prebenjamín Masculino
            'PRI_F' => '#800080', // Primera Femenino
            'PRI_M' => '#E6E6FA', // Primera Masculino
            'SEG_F' => '#FFC0CB', // Segunda Femenino
            'SEG_M' => '#FFE6F1', // Segunda Masculino
        ];

        
        $query = (new Query())
            ->select(['id_partidos','cod_categoria', 'fecha', 'hora', 'competicion', 'contrincante'])
            ->from('partidos')
            ->all();
        
        $formattedEvents = [];
        foreach ($query as $row) {
            $start = $row['fecha'] . ' ' . $row['hora'];
            $id = 'sadas' .';'. $row['id_partidos'] .';'. 'Fecha: ' . date('d-m-Y', strtotime($row['fecha'])) . "<br>Hora: " . date('H:i', strtotime($row['hora'])) . "<br>Competicion: " . $row['competicion'] . "<br>Contrincante: " . $row['contrincante'];
            $codCategoria = $row['cod_categoria'];
            $nombreCategoria = isset($categorias[$codCategoria]) ? $categorias[$codCategoria] : $codCategoria;
            
            $formattedEvents[] = new Event([
                'title' => $nombreCategoria,
                'start' => date('Y-m-d H:i', strtotime($start)),
                'id' => $id,
                'backgroundColor' => $color[$codCategoria], // Asigna el color rojo al fondo del evento
                'borderColor' => $color[$codCategoria], // Asigna el color rojo al borde del evento
            ]);
        }
        
        $dayMappings = [
            0 => 'Sunday',     // Domingo
            1 => 'Monday',     // Lunes
            2 => 'Tuesday',    // Martes
            3 => 'Wednesday',  // Miércoles
            4 => 'Thursday',   // Jueves
            5 => 'Friday',     // Viernes
            6 => 'Saturday',   // Sábado
        ];
        $dayMapping = [
            0 => 'Domingo',     // Domingo
            1 => 'Lunes',     // Lunes
            2 => 'Martes',    // Martes
            3 => 'Miércoles',  // Miércoles
            4 => 'Jueves',   // Jueves
            5 => 'Viernes',     // Viernes
            6 => 'Sábado',   // Sábado
        ];
        
        $entrenamientos = Yii::$app->db->createCommand("SELECT diasemana, hinicio, hfinal, cod_categoria FROM horarioentrenamientos")->queryAll();

        foreach ($entrenamientos as $entrenamiento) {
            $diasemana = $entrenamiento['diasemana'];
            $inicio = $entrenamiento['hinicio'];
            $fin = $entrenamiento['hfinal'];

            $startDate = date('Y-m-d', strtotime('next ' . $dayMappings[$diasemana]));
            $endDate = date('Y-m-d', strtotime('+1 year', strtotime($startDate)));

            $currentDate = $startDate;
            while (strtotime($currentDate) <= strtotime($endDate)) {
                
                $event = new Event([
                    'title' => $categorias[$entrenamiento['cod_categoria']],
                    'id' => 'entrenamiento' . ';'. $entrenamiento['cod_categoria'] .';'. $dayMapping[$diasemana],
                    'start' => $currentDate . ' ' . $inicio,
                    'end' => $currentDate . ' ' . $fin,
                    'backgroundColor' => $color[$codCategoria], // Asigna el color rojo al fondo del evento
                    'borderColor' => $color[$codCategoria], // Asigna el color rojo al borde del evento
                ]);
                
                $formattedEvents[] = $event;
                
                $currentDate = date('Y-m-d', strtotime('+1 week', strtotime($currentDate)));
            }
        }
        
       
        
         return $this->render('eventos', ['events' => $formattedEvents]);
    }
    
    public function actionGenerarpartidos(){
        $searchModel = new PartidosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('mostrarpartidos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEstadisticas(){
        
        $dineroCuotas = yii:: $app->db->createCommand(
                "select sum(importe) from cuotas"
                )->queryAll();
        
        $dineroPatrocinadores = yii:: $app->db->createCommand(
                "select sum(dinero) from patrocinadores"
                )->queryAll();
        
        $dineroEsperadocuotas = yii:: $app->db->createCommand(
                "select count(*)*380 as dinero from jugadores"
                )->queryAll();
        
        $Codcategorias = yii:: $app->db->createCommand(
                "select * from categoria"
                )->queryAll();
        
        $patrocinadorIndividual = yii:: $app->db->createCommand(
                "select nombre, dinero from patrocinadores"
                )->queryAll();
        
        $categoriaSueldo = yii:: $app->db->createCommand(
                "select cod_categoria, sum(sueldo) as sueldo from entrenadores group by cod_categoria"
                )->queryAll();
        
        $categoriaSueldo2 = yii:: $app->db->createCommand(
                "select cod_categoria, sueldo from preparadores inner join categoria using(dni) group by cod_categoria"
                )->queryAll();
        
        
        return $this->render('estadisticas', [
            'dineroCuotas' => $dineroCuotas,
            'dineroPatrocinadores' => $dineroPatrocinadores,
            'dineroEsperado' => $dineroEsperadocuotas,
            'Codcategorias' => $Codcategorias,
            'patrocinadorIndividual' => $patrocinadorIndividual,
            'categoriaSueldo' => $categoriaSueldo,
            'categoriaSueldo2' => $categoriaSueldo2  
        ]);
    }
    
    public function actionConvocatorias($id_partidos){
        
        $convocados = Jugadores::find()
            ->select(['dorsal', 'nombre', 'apellidos','id_partidos'])
            ->join('INNER JOIN', 'juegan', 'juegan.dni = jugadores.dni')
            ->where(['id_partidos' => $id_partidos])
            ->all();
        

        return $this->render('convocatoria', [
            'convocados' => $convocados,
            ]);
    }
    
    public function actionCondicioneslegales() {
        
        return $this->render('condicioneslegales');
    }
    
    public function actionPoliticadeprivacidad() {
        
        return $this->render('politicadeprivacidad');
    }
    
    public function actionProtecciondedatos() {
        
        return $this->render('protecciondedatos');
    }
    
    public function actionEstadisticascategorias(){
        
        $preparadoresTotales = yii:: $app->db->createCommand(
                "select count(*) as total from preparadores "
                )->queryAll();
        
        $jugadoresTotales = yii:: $app->db->createCommand(
                "select count(*) as total from jugadores "
                )->queryAll();
        
        $entrenadoresTotales = yii:: $app->db->createCommand(
                "select count(*) as total from entrenadores"
                )->queryAll();
        
        $directivosTotales = yii:: $app->db->createCommand(
                "select count(*) as total from directivos "
                )->queryAll();
        
        $numJugadoresCategoria = yii:: $app->db->createCommand(
                "select cod_categoria, count(*) as num_jugadores from jugadores group by cod_categoria"
                )->queryAll();
        
        $partidosGanados = yii:: $app->db->createCommand(
                "select cod_categoria, count(*) as partidoG from partidos where resultadoS > resultadoC group by cod_categoria"
                )->queryAll();
        
        $partidosPerdidos = yii:: $app->db->createCommand(
                "select cod_categoria, count(*) as partidoP from partidos where resultadoS < resultadoC group by cod_categoria"
                )->queryAll();
        
        $partidosEmpatados = yii:: $app->db->createCommand(
                "select cod_categoria, count(*) as partidoE from partidos where resultadoS = resultadoC group by cod_categoria"
                )->queryAll();
        
        $Codcategorias = yii:: $app->db->createCommand(
                "select * from categoria"
                )->queryAll();
        
        
        return $this->render('estadisticascategorias', [
            'numJugadoresCategoria' => $numJugadoresCategoria,
            'partidosEmpatados' => $partidosEmpatados,
            'partidosGanados' => $partidosGanados,
            'partidosPerdidos' => $partidosPerdidos,
            'Codcategorias' => $Codcategorias,
            'directivosTotales' => $directivosTotales,
            'jugadoresTotales' => $jugadoresTotales,
            'entrenadoresTotales' => $entrenadoresTotales,
            'preparadoresTotales' => $preparadoresTotales     
        ]);
    }
}
