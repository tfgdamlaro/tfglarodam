<?php

namespace app\controllers;

use app\models\Telefonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Jugadores;
use yii\helpers\ArrayHelper;
use app\models\TelefonosSearch;
use yii;

/**
 * TelefonosController implements the CRUD actions for Telefonos model.
 */
class TelefonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Telefonos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TelefonosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Telefonos model.
     * @param int $id_telefonos Id Telefonos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_telefonos)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_telefonos),
        ]);
    }

    /**
     * Creates a new Telefonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Telefonos();
        $data = ArrayHelper::map(Jugadores::find()->all(), 'dni', 'dni');
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_telefonos' => $model->id_telefonos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing Telefonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_telefonos Id Telefonos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_telefonos)
    {
        $model = $this->findModel($id_telefonos);
        $data = ArrayHelper::map(Jugadores::find()->all(), 'dni', 'dni');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_telefonos' => $model->id_telefonos]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Telefonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_telefonos Id Telefonos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_telefonos)
    {
        $this->findModel($id_telefonos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Telefonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_telefonos Id Telefonos
     * @return Telefonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_telefonos)
    {
        if (($model = Telefonos::findOne(['id_telefonos' => $id_telefonos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
