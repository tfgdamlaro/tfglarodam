<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinadores".
 *
 * @property int $id_patrocinadores
 * @property string|null $nombre
 * @property float|null $dinero
 * @property string|null $dni
 *
 * @property Directivos $dni0
 */
class Patrocinadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dinero'], 'number'],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['dni'], 'string', 'max' => 9],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::class, 'targetAttribute' => ['dni' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_patrocinadores' => 'Id Patrocinadores',
            'nombre' => 'Nombre',
            'dinero' => 'Dinero',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Directivos::class, ['dni' => 'dni']);
    }
}
