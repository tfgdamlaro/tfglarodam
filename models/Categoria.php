<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categoria".
 *
 * @property string $cod_categoria
 * @property string|null $dni
 *
 * @property Preparadores $dni0
 * @property Entrenadores[] $entrenadores
 * @property Jugadores[] $jugadores
 * @property Partidos[] $partidos
 */
class Categoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_categoria'], 'required'],
            [['cod_categoria'], 'string', 'max' => 5],
            [['cod_categoria'], 'match', 'pattern' => '/([A-Z])\w+\_+([M|F])/'],
            [['dni'], 'string', 'max' => 9],
            [['cod_categoria'], 'unique'],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Preparadores::class, 'targetAttribute' => ['dni' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_categoria' => 'Cod Categoria',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Preparadores::class, ['dni' => 'dni']);
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::class, ['cod_categoria' => 'cod_categoria']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::class, ['cod_categoria' => 'cod_categoria']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::class, ['cod_categoria' => 'cod_categoria']);
    }
}
