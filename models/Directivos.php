<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directivos".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $f_nacimiento
 * @property string|null $telefono
 * @property string|null $cargo
 *
 * @property Entrenadores[] $entrenadores
 * @property Patrocinadores[] $patrocinadores
 * @property Preparadores[] $preparadores
 */
class Directivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'match', 'pattern' => '/^(\d{8})([A-Z])$/'],
            [['f_nacimiento'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['nombre', 'cargo'], 'string', 'max' => 50],
            [['nombre'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['apellidos'], 'string', 'max' => 100],
            [['apellidos'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], 'match', 'pattern' => '/^\+34[6|7|8|9]\d{8}$/','message'=>'Recuerda el formato del telefono es +34 000000000 y que deben empezar por 6,7,8 o 9'],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'f_nacimiento' => 'F Nacimiento',
            'telefono' => 'Telefono',
            'cargo' => 'Cargo',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::class, ['dni_directivos' => 'dni']);
    }

    /**
     * Gets query for [[Patrocinadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinadores()
    {
        return $this->hasMany(Patrocinadores::class, ['dni' => 'dni']);
    }

    /**
     * Gets query for [[Preparadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreparadores()
    {
        return $this->hasMany(Preparadores::class, ['dni_directivos' => 'dni']);
    }
}
