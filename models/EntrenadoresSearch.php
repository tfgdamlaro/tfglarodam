<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class EntrenadoresSearch extends Entrenadores
{
    public function rules()
    {
        return [
            [['dni', 'nombre', 'apellidos', 'f_nacimiento', 'telefono', 'cod_categoria', 'dni_directivos'], 'safe'],
            [['sueldo'], 'integer'],
        ];
    }

    public function search($params)
    {
        $query = Entrenadores::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'f_nacimiento', $this->f_nacimiento])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['sueldo' => $this->sueldo])
            ->andFilterWhere(['like', 'cod_categoria', $this->cod_categoria])
            ->andFilterWhere(['like', 'dni_directivos', $this->dni_directivos]);

        return $dataProvider;
    }
}


