<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class HorarioentrenamientosSearch extends Horarioentrenamientos
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['diasemana', 'hinicio', 'hfinal', 'cod_categoria'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Horarioentrenamientos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['like', 'diasemana', $this->diasemana])
            ->andFilterWhere(['like', 'hinicio', $this->hinicio])
            ->andFilterWhere(['like', 'hfinal', $this->hfinal])
            ->andFilterWhere(['like', 'cod_categoria', $this->cod_categoria]);

        return $dataProvider;
    }
}


