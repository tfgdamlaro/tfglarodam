<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $nombre;
    public $email;
    public $titulo;
    public $informacion;
    public $codigo;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['nombre', 'email', 'informacion', 'titulo'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['codigo', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'Codigo' => 'codigo',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        $content = "<p>Email: " . $this->email . "</p>";
        $content .= "<p>Nombre: " . $this->nombre . "</p>";
        $content .= "<p>Titulo: " . $this->titulo . "</p>";
        $content .= "<p>Informacion: " . $this->informacion . "</p>";
        
        if ($this->validate()) {
            Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->nombre])
                ->setSubject($this->titulo)
                ->setTextBody($this->informacion)
                ->send();

            return true;
        }
        return false;
    }
}
