<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class JugadoresSearch extends Jugadores
{
    public function rules()
    {
        return [
            [['dni', 'nombre', 'apellidos', 'f_nacimiento', 'telefono', 'cod_categoria'], 'safe'],
            [['dorsal'], 'integer'],
            [['f_alta', 'f_baja'], 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }

    public function search($params)
    {
        $query = Jugadores::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['dorsal' => $this->dorsal])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'cod_categoria', $this->cod_categoria])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['>=', 'f_nacimiento', $this->f_nacimiento ? date('Y-m-d', strtotime($this->f_nacimiento)) : null])
            ->andFilterWhere(['>=', 'f_alta', $this->f_alta ? date('Y-m-d', strtotime($this->f_alta)) : null])
            ->andFilterWhere(['<=', 'f_baja', $this->f_baja ? date('Y-m-d', strtotime($this->f_baja)) : null]);

        return $dataProvider;
    }
}


