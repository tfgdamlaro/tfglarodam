<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "horarioentrenamientos".
 *
 * @property int $id
 * @property string|null $diasemana
 * @property string|null $hinicio
 * @property string|null $hfinal
 * @property string|null $cod_categoria
 *
 * @property Categoria $codCategoria
 */
class Horarioentrenamientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'horarioentrenamientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hinicio', 'hfinal'], 'safe'],
            [['diasemana'], 'string', 'max' => 20],
            [['cod_categoria'], 'string', 'max' => 5],
            [['cod_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::class, 'targetAttribute' => ['cod_categoria' => 'cod_categoria']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'diasemana' => 'Diasemana',
            'hinicio' => 'Hinicio',
            'hfinal' => 'Hfinal',
            'cod_categoria' => 'Cod Categoria',
        ];
    }

    /**
     * Gets query for [[CodCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCategoria()
    {
        return $this->hasOne(Categoria::class, ['cod_categoria' => 'cod_categoria']);
    }
}
