<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $f_nacimiento
 * @property string|null $telefono
 * @property int|null $sueldo
 * @property string|null $cod_categoria
 * @property string|null $dni_directivos
 *
 * @property Categoria $codCategoria
 * @property Directivos $dniDirectivos
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'match', 'pattern' => '/^(\d{8})([A-Z])$/'],
            [['f_nacimiento'], 'safe'],
            [['sueldo'], 'integer'],
            [['dni', 'dni_directivos'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Solo puedes escribir letras y espacios en blanco'],
            [['apellidos'], 'string', 'max' => 100],
            [['apellidos'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], 'match', 'pattern' => '/^\+34[6|7|8|9]\d{8}$/','message'=>'Recuerda el formato del telefono es +34 000000000 y que deben empezar por 6,7,8 o 9'],
            [['cod_categoria'], 'string', 'max' => 5],
            [['dni'], 'unique'],
            [['cod_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::class, 'targetAttribute' => ['cod_categoria' => 'cod_categoria']],
            [['dni_directivos'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::class, 'targetAttribute' => ['dni_directivos' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'f_nacimiento' => 'F Nacimiento',
            'telefono' => 'Telefono',
            'sueldo' => 'Sueldo',
            'cod_categoria' => 'Cod Categoria',
            'dni_directivos' => 'Dni Directivos',
        ];
    }

    /**
     * Gets query for [[CodCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCategoria()
    {
        return $this->hasOne(Categoria::class, ['cod_categoria' => 'cod_categoria']);
    }

    /**
     * Gets query for [[DniDirectivos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniDirectivos()
    {
        return $this->hasOne(Directivos::class, ['dni' => 'dni_directivos']);
    }
}
