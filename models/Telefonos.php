<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id_telefonos
 * @property string|null $dni
 * @property string|null $telefonos_tutores_legales
 *
 * @property Jugadores $dni0
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'string', 'max' => 9],
            [['dni'], 'match', 'pattern' => '/^(\d{8})([A-Z])$/'],
            [['telefonos_tutores_legales'], 'string', 'max' => 13],
            [['telefono_tutores_legales'], 'match', 'pattern' => '/^\+34[6|7|8|9]\d{8}$/','message'=>'Recuerda el formato del telefono es +34 000000000 y que deben empezar por 6,7,8 o 9'],
            [['telefonos_tutores_legales', 'dni'], 'unique', 'targetAttribute' => ['telefonos_tutores_legales', 'dni']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['dni' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_telefonos' => 'Id Telefonos',
            'dni' => 'Dni',
            'telefonos_tutores_legales' => 'Telefonos Tutores Legales',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Jugadores::class, ['dni' => 'dni']);
    }
}
