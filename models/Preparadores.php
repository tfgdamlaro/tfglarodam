<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preparadores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $f_nacimiento
 * @property string|null $telefono
 * @property int|null $sueldo
 * @property string|null $dni_directivos
 *
 * @property Categoria[] $categorias
 * @property Directivos $dniDirectivos
 */
class Preparadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'preparadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'match', 'pattern' => '/^(\d{8})([A-Z])$/'],
            [['f_nacimiento'], 'safe'],
            [['sueldo'], 'integer'],
            [['dni', 'dni_directivos'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['apellidos'], 'string', 'max' => 100],
            [['apellidos'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], 'match', 'pattern' => '/^\+34[6|7|8|9]\d{8}$/','message'=>'Recuerda el formato del telefono es +34 000000000 y que deben empezar por 6,7,8 o 9'],
            [['dni'], 'unique'],
            [['dni_directivos'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::class, 'targetAttribute' => ['dni_directivos' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'f_nacimiento' => 'F Nacimiento',
            'telefono' => 'Telefono',
            'sueldo' => 'Sueldo',
            'dni_directivos' => 'Dni Directivos',
        ];
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(Categoria::class, ['dni' => 'dni']);
    }

    /**
     * Gets query for [[DniDirectivos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniDirectivos()
    {
        return $this->hasOne(Directivos::class, ['dni' => 'dni_directivos']);
    }
}
