<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $id_juegan
 * @property string|null $dni
 * @property int|null $id_partidos
 *
 * @property Jugadores $dni0
 * @property Partidos $partidos
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_partidos'], 'integer'],
            [['dni'], 'string', 'max' => 9],
            [['dni', 'id_partidos'], 'unique', 'targetAttribute' => ['dni', 'id_partidos']],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['dni' => 'dni']],
            [['id_partidos'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::class, 'targetAttribute' => ['id_partidos' => 'id_partidos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_juegan' => 'Id Juegan',
            'dni' => 'Dni',
            'id_partidos' => 'Id Partidos',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Jugadores::class, ['dni' => 'dni']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasOne(Partidos::class, ['id_partidos' => 'id_partidos']);
    }
}
