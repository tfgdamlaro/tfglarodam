<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuotas".
 *
 * @property int $id_cuotas
 * @property string|null $concepto
 * @property string|null $metodo_pago
 * @property int|null $importe
 * @property string|null $dni
 *
 * @property Jugadores $dni0
 */
class Cuotas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuotas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['importe'], 'integer'],
            [['concepto'], 'string', 'max' => 200],
            [['metodo_pago'], 'string', 'max' => 50],
            [['dni'], 'string', 'max' => 9],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['dni' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cuotas' => 'Id Cuotas',
            'concepto' => 'Concepto',
            'metodo_pago' => 'Metodo Pago',
            'importe' => 'Importe',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Jugadores::class, ['dni' => 'dni']);
    }
}
