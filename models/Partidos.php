<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $id_partidos
 * @property int|null $resultadoS
 * @property int|null $resultadoC
 * @property string|null $fecha
 * @property string|null $hora
 * @property string|null $competicion
 * @property string|null $contrincante
 * @property string|null $cod_categoria
 *
 * @property Categoria $codCategoria
 * @property Jugadores[] $dnis
 * @property Juegan[] $juegans
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resultadoS', 'resultadoC'], 'integer'],
            [['fecha', 'hora'], 'safe'],
            [['competicion', 'contrincante'], 'string', 'max' => 50],
            [['cod_categoria'], 'string', 'max' => 5],
            [['cod_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::class, 'targetAttribute' => ['cod_categoria' => 'cod_categoria']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_partidos' => 'Id Partidos',
            'resultadoS' => 'Resultado S',
            'resultadoC' => 'Resultado C',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'competicion' => 'Competicion',
            'contrincante' => 'Contrincante',
            'cod_categoria' => 'Cod Categoria',
        ];
    }

    /**
     * Gets query for [[CodCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCategoria()
    {
        return $this->hasOne(Categoria::class, ['cod_categoria' => 'cod_categoria']);
    }

    /**
     * Gets query for [[Dnis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnis()
    {
        return $this->hasMany(Jugadores::class, ['dni' => 'dni'])->viaTable('juegan', ['id_partidos' => 'id_partidos']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::class, ['id_partidos' => 'id_partidos']);
    }
}
