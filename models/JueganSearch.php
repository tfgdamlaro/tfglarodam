<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class JueganSearch extends Juegan
{
    public function rules()
    {
        return [
            [['id_juegan', 'id_partidos'], 'integer'],
            [['dni'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Juegan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id_juegan' => $this->id_juegan])
            ->andFilterWhere(['id_partidos' => $this->id_partidos])
            ->andFilterWhere(['like', 'dni', $this->dni]);

        return $dataProvider;
    }
}


