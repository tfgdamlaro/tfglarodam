<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class DirectivosSearch extends Directivos
{
    public function rules()
    {
        return [
            [['dni', 'nombre', 'apellidos', 'f_nacimiento', 'telefono', 'cargo'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Directivos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'f_nacimiento', $this->f_nacimiento])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'cargo', $this->cargo]);

        return $dataProvider;
    }
}


