<?php
namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Partidos;

class PartidosSearch extends Partidos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_partidos', 'fecha', 'hora', 'competicion', 'cod_categoria','contrincante'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partidos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
        // uncomment the following line if you do not want to return any records when validation fails
        // $query->where('0=1');
        return ['dataProvider' => [$dataProvider], 'searchModel' => $this];
    }

        $query->andFilterWhere(['like', 'id_partidos', $this->id_partidos])
            ->andFilterWhere(['like', 'fecha', $this->fecha])
            ->andFilterWhere(['like', 'hora', $this->hora])
            ->andFilterWhere(['like', 'competicion', $this->competicion])
            ->andFilterWhere(['like', 'cod_categoria', $this->cod_categoria])
            ->andFilterWhere(['like', 'contrincate', $this->contrincante]);

        return $dataProvider;
    }

}
