<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $f_nacimiento
 * @property string|null $telefono
 * @property string|null $cod_categoria
 * @property int $dorsal
 * @property string|null $f_alta
 * @property string|null $f_baja
 *
 * @property Categoria $codCategoria
 * @property Cuotas[] $cuotas
 * @property Juegan[] $juegans
 * @property Partidos[] $partidos
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'match', 'pattern' => '/^(\d{8})([A-Z])$/'],
            [['f_nacimiento', 'f_alta', 'f_baja'], 'safe'],
            [['dorsal'], 'integer'],
            ['dorsal', 'match', 'pattern' => '/^(?:[1-9]|[1-9][0-9])$/',
             'message' => 'El campo debe estar entre 1 y 99.'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 50],  
            [['nombre'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['apellidos'], 'string', 'max' => 100],
            [['apellidos'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/
','message'=>'Recuerda que solo se pueden introducir letras y espacios'],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], 'match', 'pattern' => '/^\+34[6|7|8|9]\d{8}$/','message'=>'Recuerda el formato del telefono es +34 000000000 y que deben empezar por 6,7,8 o 9'],
            [['cod_categoria'], 'string', 'max' => 5],
            [['dni'], 'unique'],
            [['cod_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::class, 'targetAttribute' => ['cod_categoria' => 'cod_categoria']],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'f_nacimiento' => 'F Nacimiento',
            'telefono' => 'Telefono',
            'cod_categoria' => 'Cod Categoria',
            'dorsal' => 'Dorsal',
            'f_alta' => 'F Alta',
            'f_baja' => 'F Baja',
        ];
    }

    /**
     * Gets query for [[CodCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCategoria()
    {
        return $this->hasOne(Categoria::class, ['cod_categoria' => 'cod_categoria']);
    }

    /**
     * Gets query for [[Cuotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuotas()
    {
        return $this->hasMany(Cuotas::class, ['dni' => 'dni']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::class, ['dni' => 'dni']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::class, ['id_partidos' => 'id_partidos'])->viaTable('juegan', ['dni' => 'dni']);
    }
}
