<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
use yii\helpers\Html
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Aviso Legal</title>
</head>

<body>
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Aviso Legal</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">AVISO LEGAL</h1>
    </div>
</div>
    <p>En virtud del cumplimiento de la Ley 34/2002, de 11 de Julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, le informamos:</p>

    <ol>
        <li>
            <strong>Objeto.</strong> Art. 10 LSSI: <a href="https://sardinerohc.es">https://sardinerohc.es</a> es un dominio en internet de titularidad de SARDINERO H.C., con Domicilio Social en HERNÁN CORTÉS, 5, 1º DERECHA, 39003 SANTANDER (CANTABRIA) y CIF G39044847. A efectos de este documento el teléfono de contacto es 942 22 96 30.
            <br>
            <br>
            Este Aviso Legal regula la utilización de dicho dominio. La utilización de este sitio web implica la aceptación por parte del Usuario de las condiciones de uso incluidas en este Aviso. En el caso de que ciertos servicios contenidos y/o herramientas ofrecidos a través de este "Portal" requirieran de la aplicación condiciones particulares estas se pondrán a disposición del Usuario. Por otra parte, SARDINERO H.C. advierte que, tanto los contenidos y servicios de esta página web como las propias condiciones de utilización, pueden ser modificados sin notificación previa.<br><br>
        </li>
        <li>
            <strong>Condiciones de utilización:</strong> El Usuario se compromete a que, en los apartados en que sea necesario que se registre para poder acceder a los mismos, facilitar datos veraces, exactos y completos sobre su identidad. Además se compromete a mantener actualizados los datos personales que pudieran ser proporcionados a titular del dominio, por lo tanto, único responsable de las falsedades o inexactitudes que realice. Se informa que en caso de ser menor de edad deberá obtener el permiso de sus padres, tutores o representantes legales para poder acceder a los servicios prestados. SARDINERO H.C. no se responsabiliza en el caso de que los datos sobre este tema sean inexactos o falsos. El "portal" sólo puede ser utilizado con propósitos legales por tanto el usuario se obliga a hacer un uso lícito y honrado del portal y conforme a las presentes Condiciones Generales de Uso, a No utilizar los servicios del "portal" para la realización de actividades contrarias a las legislación española, a la moral y al orden público, asumiendo por parte del usuario todas las responsabilidades de daños y perjuicios frente al titular del dominio o terceros que pudieran derivarse de prácticas ilegales o no permitidas entres otras y a titulo enunciativo y no limitativo:
            <br>
            <br>
            <ul>
                <li>Realizar sin previo consentimiento por parte del titular del dominio cualquier manipulación o alteración de esta página, no asumiendo el titular del dominio ninguna responsabilidad que pudiera derivarse, de dicha manipulación o alteración por terceros.</
                <li>Realizar cualquier acto que pueda dañar, inutilizar, sobrecargar, o deteriorar el Portal y los servicios y/o impedir el normal uso y utilización por parte de los Usuarios- Introducir y/o Utilizar programas de ordenador, datos, archivos defectuosos, virus, código malicioso, equipos informáticos o de telecomunicaciones o cualquier otro, independientemente de su naturaleza que pueda causar daños en el Portal, en cualquiera de los servicios, o en cualesquiera activos (físicos o lógicos) de los sistemas de información de titular del dominio.</li>
                <li>Violar los derechos de terceros a la intimidad, la propia imagen, la protección de datos al secreto en las comunicaciones, a la propiedad intelectual e industrial.</li>
                <li>Ocultar y falsear el origen de mensajes de correo electrónico.</li>
                <li>Utilizar identidades falsas, suplantar la identidad de otros en la utilización del Portal o en la utilización de cualquiera de los servicios.</li>
                <li>Reproducir, distribuir, modificar o copiar el contenido de esta página, salvo que de disponga de la autorización del titular del dominio o esté legalmente autorizado.</li>
                <li>Transmitir a terceros no autorizados los nombres de Usuario y las claves de acceso.</li>
                </ul>
            <br>
            <p>SARDINERO H.C. no responde de los Enlaces (LINKS) a otras páginas de Internet de terceros y su existencia no implica que SARDINERO H.C. apruebe o acepte sus contenidos y servicios. Estas otras páginas web no están controladas por SARDINERO H.C. ni cubiertas por la presente Política de Privacidad. Si accede a otras páginas web utilizando los Links proporcionados, los operadores de dichos sitios web podrán recoger su información personal. Asegúrese de estar conforme con las Políticas de Privacidad de estas terceras páginas web antes de facilitar ningún tipo de información personal.</p>

<p>Con carácter general, el titular del dominio excluye su responsabilidad por los daños y perjuicios de cualquier naturaleza e índole que pudieran derivarse del uso del sitio web, así como por los daños y perjuicios derivados de la infracción de los derechos de propiedad Intelectual e Industrial por parte de los usuarios y/o la falta de veracidad, exactitud y actualidad de los contenidos. Además, no le podrán ser exigidas responsabilidades por la interrupción del servicio, inadecuado funcionamiento o imposibilidad de acceso al servicio.</p>

<p>El titular del dominio no será responsable de los daños y perjuicios causados por la presencia de virus o cualquier otro software lesivo que pueda producir alteraciones en el sistema informático del Usuario.</p>

<p>El sitio web, incluyendo a título enunciativo pero no limitativo, su programación, diseños, imágenes, logotipos, texto y/o gráficos son propiedad del prestador o, en su caso, dispone de licencia o autorización expresa por parte de los autores. Independientemente de la finalidad para la que fueran destinados, la reproducción total o parcial, uso, explotación, distribución y comercialización requiere en todo caso de la autorización escrita previa por parte del titular del dominio.</p>

<p>El usuario se compromete a no realizar ningún acto en contra de los derechos de propiedad intelectual o industrial del autor.</p>

<p>El prestador autoriza expresamente a que terceros puedan redirigir directamente a los contenidos concretos del sitio web, debiendo en todo caso redirigir al sitio web principal del prestador.</p>
        </li>
        <li>
            <strong>Protección de Datos:</strong>

<p>De conformidad con la vigente Ley Orgánica 15/1999 de Protección de Datos, SARDINERO H.C. informa que los datos de carácter personal de los Usuarios del sitio web se incorporarán y tratarán en un fichero propiedad de SARDINERO H.C. y que será gestionado exclusivamente para la finalidad descrita en cada formulario o medio de respuesta. Al pulsar el botón de envío en cualquiera de los formularios del presente sitio web, el Usuario consiente al tratamiento de sus datos por parte de SARDINERO H.C.</p>

        </li>

