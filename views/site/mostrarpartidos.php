<?php
    use yii\grid\GridView;
    use yii\helpers\Html;
    use kartik\date\DatePicker;
    use kartik\time\TimePicker;
    /* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Partidos</li>
  </ol>
</nav>
<div class="row centrar">
    <div class="col-md-10 pb-3">
        <h1 class="text-center titulo">PARTIDOS</h1>
    </div>
</div>
<h3 class='centrarTexto pb-3'>Partidos para crear convocatorias</h3>
<?= 
    GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
    'summary' => '',
    'filterModel' => $searchModel,
    'layout' => "{items}",
    'columns' => [
        [
            'attribute' => 'fecha',
            'format' => ['date', 'php:d/m/Y'],
            'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha',
                'language' => 'es',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])
        ],
        [
            'attribute' => 'hora',
            'format' => ['time', 'php:H:i'],
            'filter' => TimePicker::widget([
                'model' => $searchModel,
                'attribute' => 'hora',
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'defaultTime' => false,
                    'minuteStep' => 5,
                ],
            ]),
        ],
        [
            'attribute' => 'competicion',
            'filter' => true,
        ],
        [
            'attribute' => 'cod_categoria',
            'label' => 'Categoría',
            'filter' => true,
        ],
        [
            'attribute' => 'contrincante',
            'filter' => true,
        ],
        [
            'label' => 'Convocatorias',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="bi bi-card-checklist"> Crear </i>', [
                    'juegan/convocados',
                    'id_partidos' => $model['id_partidos'],
                    'codCategoria' => $model['cod_categoria'],
                ], ['class' => 'btn btn-dark']);
            }
        ]   
    ]
    ]);

?>

