<?php
use yii\helpers\Html;

?>
<div class="row centrar pb-2">
    <div class="col-md-10">
        <h1 class="text-center titulo2">SARDINERO HC<?= Html::img('@web/img/sardinero.png', ['alt' => Yii::$app->name, 'class' => 'pl-3 pt-3']) ?></h1>
    </div>
</div>
<div class="centrar">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
             <?= Html::img('@web/img/c1.jpg', ['alt' => Yii::$app->name]) ?>
          </div>
          <div class="carousel-item">
             <?= Html::img('@web/img/c2.jpg', ['alt' => Yii::$app->name]) ?>
          </div>
          <div class="carousel-item">
            <?= Html::img('@web/img/c3.jpg', ['alt' => Yii::$app->name]) ?>
          </div>
        </div>
    </div>
</div>

<div class="pt-5 pb-3">
    <div class="container">
        <div class="row">
            <div class="pl-5 pr-5">
                <!-- Contenido del primer componente -->
                <div class="d-flex flex-column justify-content-center align-items-center">
                    <?= Html::img('@web/img/02-Unicaja-Banco-Vertical-300x279.jpg', ['alt' => Yii::$app->name]) ?>
                    <?= Html::img('@web/img/cantabria-deporte-300x101.jpg', ['alt' => Yii::$app->name, 'class' => ' pt-2']) ?>
                    <?= Html::img('@web/img/jubilar-rgb-horizontal-300x141.jpg', ['alt' => Yii::$app->name, 'class' => ' pt-2']) ?>
                    <?= Html::img('@web/img/iberdrola-300x139.png', ['alt' => Yii::$app->name, 'class' => ' pt-2']) ?>                    
                </div>
            </div>
            <div class="noticias">
                <!-- Contenido del segundo componente -->
                <body data-spy="scroll" data-target="#navbar-example">
                    <div class="container">
                        <div class="row">
                            <div class="mx-auto">
                                <nav id="navbar-example" class="navbar noticias-custom rounded">
                                    <a class="navbar-brand notias-titulo">Noticias</a>
                                    <ul class="nav nav-pills">
                                        <?php foreach ($ultimaNoticia as $index => $noticia): ?>
                                            <li class="nav-item">
                                                <a class="nav-link btn btn-outline-light" href="#section<?= $index + 1 ?>"><?= Html::encode($noticia->titulo) ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </nav>

                                <div class="scrollspy-example">
                                    <?php foreach ($ultimaNoticia as $index => $noticia): ?>
                                        <h4 id="section<?= $index + 1 ?>"><?= Html::encode($noticia->titulo) ?></h4>
                                        <p><?= nl2br(Html::encode($noticia->contenido)) ?></p>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </body>
            </div>
        </div>
    </div>
</div>







