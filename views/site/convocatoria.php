<?php 

use yii\grid\GridView;
use yii\helpers\Html;

?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item"><?=Html::a('Eventos',['site/mostrarentrenamientos'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Convocatoria</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">CONVOCATORIA</h1>
    </div>
</div>
<?php
echo GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $convocados,
        'pagination' => false,
    ]),
    'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
    'summary' => '',
    'emptyText' => 'No hay jugadores convocados. Por favor, crea una convocatoria.',
    'columns' => [
        'nombre',
        'apellidos',
        'dorsal',
    ],
]);
?>
