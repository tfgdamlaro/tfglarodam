<?php
    use yii\grid\GridView;
    use yii\widgets\Breadcrumbs;
    use yii\helpers\Html;
    $categorias = [
            'AL_F' => 'Alevín Femenino',
            'AL_M' => 'Alevín Masculino',
            'BN_F' => 'Benjamín Femenino',
            'BN_M' => 'Benjamín Masculino',
            'CAD_F' => 'Cadete Femenino',
            'CAD_M' => 'Cadete Masculino',
            'DH_F' => 'Division de Honor Femenino',
            'DH_M' => 'Division de Honor Masculino',
            'IF_F' => 'Infantil Femenino',
            'IF_M' => 'Infantil Masculino',
            'JUV_F' => 'Juvenil Femenino',
            'JUV_M' => 'Juvenil Masculino',
            'PB_F' => 'Prebenjamín Femenino',
            'PB_M' => 'Prebenjamín Masculino',
            'PRI_F' => 'Primera Femenino',
            'PRI_M' => 'Primera Masculino',
            'SEG_F' => 'Segunda Femenino',
            'SEG_M' => 'Segunda Masculino',
        ];
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item"><?=Html::a('Categorias',['site/infocategoria'])?></li>
    <li class="breadcrumb-item active" aria-current="page"><?= $categorias[$codCategoria] ?></li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo"><?= $categorias[$codCategoria] ?></h1>
    </div>
</div>
        <?php 

    $cont = 0;
    foreach($preparadores as $key => $dato){
        $nombreP[] = ($dato['nombre']);
        $apellidosP[] = ($dato['apellidos']);
        $telefonoP[] = ($dato['telefono']);
                        
        echo"<div class='col-md-6 mx-auto'>
                <div class='card alturaminima border border-dark'>
                    <div class='card-body tarjeta'>".
                        "<h3 class='tituloEquipo'>Preparador Fisico</h3>                  
                        <p> Nombre: ".$nombreP[$cont]."<br>
                        Apellidos: ".$apellidosP[$cont]."<br>
                        Telefono: <a href='tel:" . $telefonoP[$cont] . "'>" . $telefonoP[$cont] . "</a></p>                   
                    </div>
                </div>
            </div>";
    }
?>
<div class="body-content">
    <div class="row">
<?php
    foreach ($entrenadores as $key => $dato) {
        $nombreE[] = ($dato['nombre']);
        $apellidosE[] = ($dato['apellidos']);
        $telefonoE[] = ($dato['telefono']);

        echo"<div class='col-md-6'>
                <div class='card alturaminima border border-dark'>
                    <div class='card-body tarjeta'>" .
                        "<h3 class='tituloEquipo'>Entrenador</h3>
                        <p> Nombre: " . $nombreE[$cont] . "<br>
                        Apellidos: " . $apellidosE[$cont] . "<br>
                        Telefono: <a href='tel:" . $telefonoE[$cont] . "'>" . $telefonoE[$cont] . "</a></p>
                    </div>
                </div>
            </div>";
        $cont++;
    }
?>
    </div>
</div>    

<div class="pt-3">
    <?=
        GridView::widget([
                    'dataProvider' => $jugadores,
                    'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
                    'summary' => '',
                    'layout' => "{items}",
                    'columns' => ['nombre','apellidos','dorsal']
        ]);
    ?>
</div>

