<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Nuestro Club';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">HISTORIA DEL SARDINERO H.C.</h1>
    </div>
</div>
<!DOCTYPE html>
<html>
<head>
    <title>Historia del Sardinero H.C.</title>
</head>
<body>
    <div class="float-left">
         <?= Html::img('@web/img/historia1.jpg', ['alt' => Yii::$app->name]) ?>
    </div>
    <div class="float-right">
        <?= Html::img('@web/img/historia2.jpg', ['alt' => Yii::$app->name]) ?>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<p>En el año 1984, un grupo de amigos, apasionados por el hockey hierba, funda el Sardinero H.C. Inicialmente su estructura es la de un club de hockey masculino formado por jugadores juveniles e infantiles de la “cantera” del Colegio Calasanz y procedentes de La Pasiega H.C.</p>
<p>Es en la temporada 1987/88, cuando se crea la sección de hockey femenino, convirtiéndose en parte importante de la historia de nuestro club. Un equipo, que llegó a ser subcampeón de la Copa de la Reina y que, además, consiguió jugar la Recopa de Europa, quedando en 5º lugar.</p>
<p>Posteriormente en la temporada 1992/93, se logra un acuerdo de patrocinio con Caja Cantabria, pasándose a denominar Caja Cantabria Sardinero, vigente hasta el año 2012 en el que finalizó dicho convenio.</p>
<p>Desde el principio el Sardinero H.C. ha contado con equipos de todas las categorías tanto masculina como femenina (benjamines, alevines, infantiles, juveniles y absoluta). Actualmente, la Escuela de Hockey, formada por más de 200 jóvenes de edades comprendidas entre los 5 y 15 años, está dirigida por Oscar Barrena y Alejandro Siri, en colaboración con el Instituto Municipal de Deportes. El número de equipos federados tanto en categorías inferiores como seniors suma un total de 30, lo que hace que para la temporada 2017-2018 tengamos más de 400 jugadores federados. ¡Quién lo iba a decir 34 años después!</p>
<p>Nuestras categorías inferiores, equipos infantiles, cadetes y juveniles, tanto masculinos como femeninos, han participado a lo largo de la historia del Club en múltiples Campeonatos de España y fases de sector en las modalidades de hierba y sala, consiguiendo grandes resultados deportivos, así como medallas en las fases finales.</p>
<p>Además, durante toda su historia el Sardinero H.C., ha sido, es y será un gran dinamizador del hockey hierba tanto a nivel regional como nacional.</p>
<p>Hemos organizado numerosos sectores y fases finales de Campeonatos de España en las categorías masculina y femenina tanto en sala como en hierba. De igual manera, hemos organizado dos fases finales de la Copa de la Reina y colaborado con la Federación Cántabra de Hockey y la Federación Española de Hockey en torneos internacionales de prestigio, como por ejemplo, el Campeonato de Europa de hockey sala en el año 2004 o el Torneo Preolímpico 3 Naciones celebrado en Santander en el año 2012 o el Campeonato de Europa Sub-18 que se celebrará este año 2018 en Santander, entre otros.</p>
<p>Desde el año 1982 el Calasanz H.C., y posteriormente el Sardinero H.C., organizamos el Memorial Carlos Cagigal, en recuerdo de un jugador juvenil fallecido en accidente. Un torneo que se ha convertido en referencia dentro del ámbito del hockey y que tradicionalmente ha reunido a los mejores equipos nacionales e internacionales. Este año se ha celebrado la XXXVII edición de este Torneo en el que han participado equipos nacionales y extranjeros, tanto en categoría masculina como femenina.</p>
<p>Asimismo, se organiza a finales de temporada el Torneo Sardinero en el que participan los equipos de la cantera del club (desde prebenjamines hasta infantiles). Un torneo que ya va por su 14ª edición y que reúne cada año, a más de 550 jugadores procedentes de diferentes equipos nacionales e internacionales.</p>
<p>Pero además de la labor formativa y dinamizadora, hemos aportado jugadores internacionales en todas las categorías (sub-16, sub-18, sub-21 y absolutas) tanto masculinas como femeninas en las modalidades de hierba y sala. Muchos de estos jugadores han participado en campeonatos europeos, mundiales, Champions Trophy o Juegos Olímpicos, consiguiendo importantes triunfos mientras vestían la camiseta de nuestro Club.</p>
<p>Técnicos y jefes de equipo del Sardinero H.C. han formado parte de las diferentes selecciones nacionales de hierba y sala: Pablo Galán, Angel Laso, Alejandro Siri, Bernardino Herrera, Antonio González o Mar Feito, lo mismo que los árbitros internacionales Belén González, Ana Fernández y Carlos Rodríguez.</p>
<p>De nuestro Club, han salido directivos con cargos relevantes en la Federaciones Española y Europea (José Manuel Trueba, Pablo Galán, etc.).</p>
<p>Lo que empezó siendo un club familiar, a día de hoy, se ha convertido en una entidad deportiva con una estructura consolidada, que cuenta con tres equipos en categoría nacional (División de Honor B masculina, Primera División masculina y Primera División femenina).</p>
<p>Con el paso de los años, nuestro club se ha convertido en parte importante del hockey español en todos los estamentos, tanto deportivo, como técnico y ejecutivo.</p>
<p>La historia del Sardinero HC, nuestro club, irá ligada siempre al esfuerzo, al trabajo y al espíritu de lucha. Tres premisas con las que trabajamos día a día y con las que hemos conseguido consolidar esta pequeña gran familia que se ha convertido en un clásico dentro del hockey español.</p>
</body>
</html>
