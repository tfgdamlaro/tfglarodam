<?php
use yii\helpers\Html;
use yii\widgets\ListView;
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
 $cont = 0;
 $categorias = [
            'AL_F' => 'Alevín Femenino',
            'AL_M' => 'Alevín Masculino',
            'BN_F' => 'Benjamín Femenino',
            'BN_M' => 'Benjamín Masculino',
            'CAD_F' => 'Cadete Femenino',
            'CAD_M' => 'Cadete Masculino',
            'DH_F' => 'Division de Honor Femenino',
            'DH_M' => 'Division de Honor Masculino',
            'IF_F' => 'Infantil Femenino',
            'IF_M' => 'Infantil Masculino',
            'JUV_F' => 'Juvenil Femenino',
            'JUV_M' => 'Juvenil Masculino',
            'PB_F' => 'Prebenjamín Femenino',
            'PB_M' => 'Prebenjamín Masculino',
            'PRI_F' => 'Primera Femenino',
            'PRI_M' => 'Primera Masculino',
            'SEG_F' => 'Segunda Femenino',
            'SEG_M' => 'Segunda Masculino',
        ];
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Categorias</li>
  </ol>
</nav>
<div class="row centrar">
    <div class="col-md-10">
        <h1 class="text-center titulo">INFORMACIÓN CATEGORÍAS</h1>
    </div>
</div>
<div class="body-content">
    <div class="row">
        <?php foreach($codCategoriaM as $key => $dato){
            $femenino[]=($codCategoriaF[$key]['cod_categoria']);
            $masculino[]=($dato['cod_categoria']);
            
            echo"<div class='col-md-6'>
                <div class='card alturaminima border border-dark'>
                    <div class='card-body tarjeta'>".
                        "<h3 class='tituloEquipo'>".$categorias[$masculino[$cont]]."</h3>".
                        "<p>".$masculino[$cont]."</p>".
                        "<p>".
                            Html::a('Datos de la categoría', ['site/datoscategoria', 'codCategoria' => $masculino[$cont]], ['class' => 'btn btn-dark']) 
                        ."</p>".
                    "</div>
                 </div>
            </div>".
            
            "<div class='col-md-6'>
                <div class='card alturaminima border border-dark'>
                    <div class='card-body tarjeta'>".
                        "<h3 class='tituloEquipo'>".$categorias[$femenino[$cont]]."</h3>".
                        "<p>".$femenino[$cont]."</p>".
                        "<p>".
                            Html::a('Datos de la categoría', ['site/datoscategoria', 'codCategoria' => $femenino[$cont]], ['class' => 'btn btn-dark']) 
                        ."</p>".
                    "</div>
                 </div>
            </div>";
            $cont++;
        }    
        ?>
    </div>
</div>



