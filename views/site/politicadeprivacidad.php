<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
use yii\bootstrap4\Html;
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Politica de Privacidad</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">POLÍTICA DE PRIVACIDAD</h1>
    </div>
</div>
<h2>Información en cumplimiento de la normativa de protección de datos personales</h2>

<p>En Europa y en España existen normas de protección de datos pensadas para proteger su información personal de obligado cumplimiento para nuestra entidad.</p>

<p>Por ello, es muy importante para nosotros que entienda perfectamente qué vamos a hacer con los datos personales que le pedimos.</p>

<p>Así, seremos transparentes y le daremos el control de sus datos, con un lenguaje sencillo y opciones claras que le permitirán decidir qué haremos con su información personal.</p>

<p>Por favor, si una vez leída la presente información le queda alguna duda, no dude en preguntarnos.</p>

<p>Muchas gracias por su colaboración.</p>

<h3>¿Quiénes somos?</h3>

<p>Nuestra denominación: Sardinero, HC</p>

<p>Nuestro CIF / NIF: G39044847</p>

<p>Nuestra actividad principal: Desarrollo de actividades deportivas, especialmente hockey sobre hierba.</p>

<p>Nuestra dirección: HERNÁN CORTÉS, 5, 2º DERECHA, CP 39003, Santander (Cantabria)</p>

<p>Nuestro teléfono de contacto: 942229690</p>

<p>Nuestra dirección de correo electrónico de contacto: informacionsardinerohc@gmail.com</p>

<p>Nuestra página web: <a href="https://sardinerohc.es/">https://sardinerohc.es/</a></p>

<p>Estamos a su disposición, no dude en contactar con nosotros.</p>

<h3>¿Para qué vamos a usar sus datos?</h3>

<p>Con carácter general, sus datos personales serán usados para poder relacionarnos con usted y poder prestarle nuestros servicios.</p>

<p>Asimismo, también pueden ser usados para otras actividades, como enviarle publicidad o promocionar nuestras actividades.</p>

<h3>¿Por qué necesitamos usar sus datos?</h3>

<p>Sus datos personales son necesarios para poder relacionarnos con usted y poder prestarle nuestros servicios. En este sentido, pondremos a su disposición una serie de casillas que le permitirán decidir de manera clara y sencilla sobre el uso de su información personal.</p>

<h3>¿Quién va a conocer la información que le pedimos?</h3>

<p>Con carácter general, sólo el personal de nuestra entidad que esté debidamente autorizado podrá tener conocimiento de la información que le pedimos.</p>

<p>De igual modo, podrán tener conocimiento de su información personal aquellas entidades que necesiten tener acceso a la misma para que podamos prestarle nuestros servicios. Así por ejemplo, nuestro banco conocerá sus datos si el pago de nuestros servicios se realiza mediante tarjeta o transferencia bancaria.</p>

<p>Asimismo, tendrán conocimiento de su información aquellas entidades públicas o privadas a las cuales estemos obligados a facilitar sus datos personales con motivo del cumplimiento de alguna ley. Poniéndole un ejemplo, la Ley Tributaria obliga a facilitar a la Agencia Tributaria determinada información sobre operaciones económicas que superen una determinada cantidad.</p>

<p>En el caso de que, al margen de los supuestos comentados, necesitemos dar a conocer su información personal a otras entidades, le solicitaremos previamente su permiso a través de opciones claras que le permitirán decidir a este respecto.</p>

<h3>¿Cómo vamos a proteger sus datos?</h3>

<p>Protegeremos sus datos con medidas de seguridad eficaces en función de los riesgos que conlleve el uso de su información.</p>

<p>Para ello, nuestra entidad ha aprobado una Política de Protección de Datos y se realizan controles y auditorías anuales para verificar que sus datos personales están seguros en todo momento.</p>

<h3>¿Enviaremos sus datos a otros países?</h3>

<p>En el mundo hay países que son seguros para sus datos y otros que no lo son tanto. Así por ejemplo, la Unión Europea es un entorno seguro para sus datos. Nuestra política es no enviar su información personal a ningún país que no sea seguro desde el punto de vista de la protección de sus datos.</p>

<p>En el caso de que, con motivo de prestarle el servicio, sea imprescindible enviar sus datos a un país que no sea tan seguro como España, siempre le solicitaremos previamente su permiso y aplicaremos medidas de seguridad eficaces que reduzcan los riesgos del envío de su información personal a otro país.</p>

<h3>¿Durante cuánto tiempo vamos a conservar sus datos?</h3>

<p>Conservaremos sus datos durante nuestra relación y mientras nos obliguen las leyes. Una vez finalizados los plazos legales aplicables, procederemos a eliminarlos de forma segura y respetuosa con el medio ambiente.</p>

<h3>¿Cuáles son sus derechos de protección de datos?</h3>

<p>En cualquier momento puede dirigirse a nosotros para saber qué información tenemos sobre usted, rectificarla si fuese incorrecta y eliminarla una vez finalizada nuestra relación, en el caso de que ello sea legalmente posible.</p>

<p>También tiene derecho a solicitar el traspaso de su información a otra entidad. Este derecho se llama “portabilidad” y puede ser útil en determinadas situaciones.</p>

<p>Para solicitar alguno de estos derechos, deberá realizar una solicitud escrita a nuestra dirección, junto con una fotocopia de su DNI, para poder identificarle.</p>

<p>En las oficinas de nuestra entidad disponemos de formularios específicos para solicitar dichos derechos y le ofrecemos nuestra ayuda para su cumplimentación.</p>

<p>Para saber más sobre sus derechos de protección de datos, puede consultar la página web de la Agencia Española de Protección de Datos (<a href="http://www.agpd.es/">www.agpd.es</a>).</p>

<h3>¿Puede retirar su consentimiento si cambia de opinión en un momento posterior?</h3>

<p>Usted puede retirar su consentimiento si cambia de opinión sobre el uso de sus datos en cualquier momento.</p>

<p>Así por ejemplo, si usted en su día estuvo interesado/a en recibir publicidad de nuestros productos o servicios, pero ya no desea recibir más publicidad, puede hacérnoslo constar a través del formulario de oposición al tratamiento disponible en las oficinas de nuestra entidad.</p>

<h3>En caso de que entienda que sus derechos han sido desatendidos, ¿dónde puede formular una reclamación?</h3>

<p>En caso de que entienda que sus derechos han sido desatendidos por nuestra entidad, puede formular una reclamación en la Agencia Española de Protección de Datos, a través de alguno de los medios siguientes:</p>

<p>Sede electrónica: <a href="http://www.agpd.es/">www.agpd.es</a></p>
<p>Dirección postal:</p>
<p>Agencia Española de Protección de Datos</p>
<p>C/ Jorge Juan, 6</p>
<p>28001-Madrid</p>

<p>Vía telefónica:</p>
<p>Telf. 901 100 099</p>
<p>Telf. 91 266 35 17</p>

<p>Formular una reclamación en la Agencia Española de Protección de Datos no conlleva ningún coste y no es necesaria la asistencia de abogado ni procurador.</p>

<h3>¿Elaboraremos perfiles sobre usted?</h3>

<p>Nuestra política es no elaborar perfiles sobre los usuarios de nuestros servicios.</p>

<p>No obstante, pueden existir situaciones en las que, con fines de prestación del servicio, comerciales o de otro tipo, necesitemos elaborar perfiles de información sobre usted. Un ejemplo pudiera ser la utilización de su historial de compras o servicios para poder ofrecerle productos o servicios adaptados a sus gustos o necesidades.</p>

<p>En tal caso, aplicaremos medidas de seguridad eficaces que protejan su información en todo momento de personas no autorizadas que pretendan utilizarla en su propio beneficio.</p>

<h3>¿Usaremos sus datos para otros fines?</h3>

<p>Nuestra política es no usar sus datos para otras finalidades distintas a las que le hemos explicado. Si, no obstante, necesitásemos usar sus datos para actividades distintas, siempre le solicitaremos previamente su permiso a través de opciones claras que le permitirán decidir al respecto.</p>

</body>
</html>

