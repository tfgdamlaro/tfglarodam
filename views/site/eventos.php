<?php
    use edofre\fullcalendar\Fullcalendar;
    use edofre\fullcalendar\models\Event;
    use yii\bootstrap4\Html;
    
?>
    
<?php
    $categorias = [
        'AL_F' => ['nombre' => '<span style="color:  #000000;">ALEVÍN FEMENINO</span>', 'color' => '#FF0000'],
        'AL_M' => ['nombre' => '<span style="color: #000000;">ALEVÍN MASCULINO</span>', 'color' => '#FFCCCC'],
        'BN_F' => ['nombre' => '<span style="color: #000000;">BENJAMÍN FEMENINO</span>', 'color' => '#0000FF'],
        'BN_M' => ['nombre' => '<span style="color: #000000;">BENJAMÍN MASCULINO</span>', 'color' => '#CCCCFF'],
        'CAD_F' => ['nombre' => '<span style="color: #000000;">CADETE FEMENINO</span>', 'color' => ': #00FF00'],
        'CAD_M' => ['nombre' => '<span style="color: #000000;">CADETE MASCULINO</span>', 'color' => '#CCFFCC'],
        'IF_F' => ['nombre' => '<span style="color: #000000;">INFANTIL FEMENINO</span>', 'color' => ' #FFFF00'],
        'IF_M' => ['nombre' => '<span style="color: #000000;">INFANTIL MASCULINO</span>', 'color' => '#FFFFCC'],
        'JUV_F' => ['nombre' => '<span style="color: #000000;">JUVENIL FEMENINO</span>', 'color' => '#FFA500'],
        'JUV_M' => ['nombre' => '<span style="color: #000000;">JUVENIL MASCULINO</span>', 'color' => '#FFDAB9'],
        'PRI_F' => ['nombre' => '<span style="color: #000000;">PRIMERA FEMENINO</span>', 'color' => '#800080'],
        'PRI_M' => ['nombre' => '<span style="color: #000000;">PRIMERA MASCULINO</span>', 'color' => ' #E6E6FA'],
        'SEG_F' => ['nombre' => '<span style="color: #000000;">SEGUNDA FEMENINO</span>', 'color' => '#FFC0CB'],
        'SEG_M' => ['nombre' => '<span style="color: #000000;">SEGUNDA MASCULINO</span>', 'color' => '#FFE6F1'],
        'PB_F' => ['nombre' => '<span style="color: #000000;">PREBENJAMÍN FEMENINO</span>', 'color' => '#00FFFF'],
        'PB_M' => ['nombre' => '<span style="color: #000000;">PREBENJAMÍN MASCULINO</span>', 'color' => '#E0FFFF'],
        'DH_F' => ['nombre' => '<span style="color: #000000;">DIVISIÓN DE HONOR FEMENINO</span>', 'color' => '#A52A2A'],
        'DH_M' => ['nombre' => '<span style="color: #000000;">DIVISIÓN DE HONOR MASCULINO</span>', 'color' => '#D2B48C'],
    ];

?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mb-5">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Programcion</li>
  </ol>
</nav>
<div class='pb-5'>
    <?= Fullcalendar::widget([
    'options' => [
        'id' => 'calendar',
    ],
    'clientOptions' => [
        'eventClick' => new \yii\web\JsExpression('function(event) {
            showEventModal(event);
        }'),
        'timeFormat' => 'H:mm',
    ],
    'header' => [
            'left' => 'prev,next',
            'center' => 'title',
            'right' => 'today',
        ],
    'events' => $events,
]); ?>

<?php
    echo '<div class="cuadrados">';

    $contador = 0;
    foreach ($categorias as $categoria => $data) {
        if (isset($data['nombre']) && isset($data['color'])) {
            $nombre = $data['nombre'];
            $color = $data['color'];
            $colorBox = '<span style="display: inline-block; width: 10px; height: 10px; background-color: ' . $color . ';"></span>';
            echo '<div style="display: flex; align-items: center;">';
            echo $colorBox . '<span style="margin-left: 10px;">' . $nombre . '</span>';
            echo '</div>';

            $contador++;
            if ($contador % 16 == 0) {
                echo '<br>';
            }
        }
    }

    echo '</div>';

?>

<!-- Modal -->
<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="eventModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header card-body">
                <h4 class="modal-title card-body" id="eventModalLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body card-body"></div>
            <div class="modal-footer card-body">
                <a href="#" id="convocatoriaLink" class="btn btn-dark">Información</a>
            </div>
        </div>
    </div>
</div>

<script>
    function showEventModal(event) {
    var des = event.id.split(";");

    if (des[0] === 'entrenamiento') {

      var title = "Entrenamiento: " + event.title;
      var start = moment(event.start).format('HH:mm');
      var end = moment(event.end).format('HH:mm');
      var description = "Día de la semana: " + des[2] + "<br>Hora inicio: " + start + "<br>Hora final: " + end;

      console.log("title:", title);
      console.log("description:", description);

      $("#eventModal .modal-title").text(title);
      $("#eventModal .modal-body").html(description);

      var convocatoriaLink = $("#convocatoriaLink");
      var convocatoriaUrl = "datoscategoria?codCategoria=" + des[1];
      convocatoriaLink.attr("href", convocatoriaUrl);

      $("#eventModal").modal("show");
    } else {
        
      var title = event.title;
      var detalles = des[2];

      console.log("title:", title);
      console.log("detalles:", detalles);

      $("#eventModal .modal-title").text(title);
      $("#eventModal .modal-body").html(detalles);

      var convocatoriaLink = $("#convocatoriaLink");
      var convocatoriaUrl = "convocatorias?id_partidos=" + des[1];
      convocatoriaLink.attr("href", convocatoriaUrl);

      $("#eventModal").modal("show");
    }
}



</script>



