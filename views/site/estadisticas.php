<?php
use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;
    use yii\helpers\Html;



$data = [
    ['Cuotas', (float)$dineroCuotas[0]['sum(importe)']],
    ['Patrocinadores', (float)$dineroPatrocinadores[0]['sum(dinero)']],
];
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Estadisticas</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">DISTRIBUCIÓN DE INGRESOS Y GASTOS</h1>
    </div>
</div>
<div>
    <div class="row">
        <div class='col-md-6'>
            <?php 
                echo Highcharts::widget([
                    'options' => [
                        'title' => ['text' => 'Distribución de ingresos de cuotas y patrocinadores'],
                        'credits' => [
                            'enabled' => false
                        ],
                        'series' => [
                            [
                            'type' => 'pie',
                            'name' => 'Ingresos',
                            'data' => $data,
                            ],
                        ], 
                    ],
                ]);
            ?>
        </div>
        <div class='col-md-6'>
            <?php 
                $data1 = [
                    ['Ingresos recibidos', (float)$dineroCuotas[0]['sum(importe)']],
                    ['Ingresos esperados', (float)$dineroEsperado[0]['dinero']],
                ];
                 echo Highcharts::widget([
                    'options' => [
                        'title' => ['text' => 'Distribución de ingresos esperados y recibidos de las cuotas'],
                        'credits' => [
                            'enabled' => false
                        ],
                        'series' => [
                            [
                            'type' => 'pie',
                            'name' => 'Ingresos',
                            'data' => $data1,
                            ],
                        ], 
                    ],
                ]);
            ?>
        </div>
        <div class='col-md-6 pt-4'>
            <?php 
                $patrocinadoresData = [];
                foreach ($patrocinadorIndividual as $patrocinador) {
                    $patrocinadoresData[] = [
                        'name' => $patrocinador['nombre'],
                        'y' => (float) $patrocinador['dinero']
                    ];
                }
                echo Highcharts::widget([
                    'options' => [
                        'chart' => [
                            'type' => 'pie'
                        ],
                        'title' => ['text' => 'Ingresos de Patrocinadores'],
                        'tooltip' => [
                            'pointFormat' => '<b>{point.name}</b>: € {point.y:.2f}'
                        ],
                        'credits' => [
                            'enabled' => false
                        ],
                        'plotOptions' => [
                            'pie' => [
                                'allowPointSelect' => true,
                                'cursor' => 'pointer',
                                'dataLabels' => [
                                    'enabled' => true,
                                    'format' => '<b>{point.name}</b>: € {point.y:.2f}'
                                ]
                            ]
                        ],
                        'series' => [
                            [
                                'name' => 'Patrocinadores',
                                'data' => $patrocinadoresData
                            ]
                        ]
                    ]
                ]);
            ?>
        </div>
        <div class='col-md-6 pt-4'>
            <?php 
                foreach ($categoriaSueldo as $item) {
                    $categories[] = $item['cod_categoria'];
                    $seriesData[$item['cod_categoria']] = (float) $item['sueldo'];
                }

                foreach ($categoriaSueldo2 as $item) {
                    if (isset($seriesData[$item['cod_categoria']])) {
                        $seriesData[$item['cod_categoria']] += (float) $item['sueldo'];
                    } else {
                        $categories[] = $item['cod_categoria'];
                        $seriesData[$item['cod_categoria']] = (float) $item['sueldo'];
                    }
                }

                echo Highcharts::widget([
                    'options' => [
                        'chart' => [
                            'type' => 'column'
                        ],
                        'credits' => [
                            'enabled' => false
                        ],
                        'title' => ['text' => 'Gráfica de coste entrenadores y preparadores por Categoría'],
                        'xAxis' => [
                            'categories' => $categories
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'Sueldo']
                        ],
                        'series' => [
                            [
                                'name' => 'Coste',
                                'data' => array_values($seriesData)
                            ]
                        ]
                    ]
                ]);
            ?>
        </div>
        </div>

