<?php 
    use miloschuman\highcharts\Highcharts;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Estadísticas</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">INFORMACIÓN CATEGORÍAS</h1>
    </div>
</div>
        <div class="row">
            <div class='col-md-6'>
                <?php 
                    foreach ($numJugadoresCategoria as $key => $values) {
                        $categorias[] = ($values['cod_categoria']);
                        $numJugadores[] = intval($values['num_jugadores']);       
                    }

                    echo Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => [
                               'type' => 'column'
                            ],
                            'credits' => [
                                'enabled' => false
                            ],
                           'title' => ['text' => 'Total de jugadores por categoría'],
                           'xAxis' => [
                               'categories' => $categorias,
                           ],
                           'yAxis' => [
                               'title' => ['text' => 'Número de jugadores'],
                           ],
                           'series' => [
                               [
                                   'name' => 'Jugadores',
                                   'data' => $numJugadores,
                               ],
                           ],
                       ]
                   ]);
                ?>
            </div>
            <div class='col-md-6'>
                <?php 
                    $personalesTotalesData = [
                        ['Directivos', intval($directivosTotales[0]['total'])],
                        ['Jugadores', intval($jugadoresTotales[0]['total'])],
                        ['Entrenadores', intval($entrenadoresTotales[0]['total'])],
                        ['Preparadores', intval($preparadoresTotales[0]['total'])],
                    ];

                    echo Highcharts::widget([
                        'options' => [
                            'title' => ['text' => 'Total de personas por puesto'],
                            'credits' => [
                                'enabled' => false
                            ],
                            'series' => [
                                [
                                'type' => 'pie',
                                'name' => 'total',
                                'data' => $personalesTotalesData,
                                ],
                            ], 
                        ],
                    ]);
                ?>
            </div>
        </div>    
<?php 
 

$Codcategoria = [];
$partidosGanadosData = [];
$partidosPerdidosData = [];
$partidosEmpatadosData = [];

foreach ($Codcategorias as $key => $values) {
    $Codcategoria[] = ($values['cod_categoria']);
    if (!isset($partidosGanadosData[$values['cod_categoria']])) {
        $partidosGanadosData[$values['cod_categoria']] = 0;
    }

    if (!isset($partidosPerdidosData[$values['cod_categoria']])) {
        $partidosPerdidosData[$values['cod_categoria']] = 0;
    }

    if (!isset($partidosEmpatadosData[$values['cod_categoria']])) {
        $partidosEmpatadosData[$values['cod_categoria']] = 0;
    }   
}

foreach ($partidosGanados as $partido) {
    $partidosGanadosData[$partido['cod_categoria']] = intval($partido['partidoG']);
}

foreach ($partidosPerdidos as $partido) {
    $partidosPerdidosData[$partido['cod_categoria']] = intval($partido['partidoP']);
}

foreach ($partidosEmpatados as $partido) {
    $partidosEmpatadosData[$partido['cod_categoria']] = intval($partido['partidoE']);
}
?>
<div class="pt-3">
    <?php
    echo Highcharts::widget([
            'options' => [
            'chart' => [
                'type' => 'column'
            ],
            'credits' => [
                'enabled' => false
            ],
            'title' => ['text' => 'Estadísticas de los partidos por categoría'],
            'xAxis' => [
                'categories' => $Codcategoria,
            ],
            'yAxis' => [
                'title' => ['text' => 'Cantidad de partidos']
            ],
            'series' => [
                [
                    'name' => 'Partidos Ganados',
                    'data' => array_values($partidosGanadosData)
                ],
                [
                    'name' => 'Partidos Perdidos',
                    'data' => array_values($partidosPerdidosData)
                ],
                [
                    'name' => 'Partidos Empatados',
                    'data' => array_values($partidosEmpatadosData)
                ]
            ]
        ]
    ]);
    ?>
</div>
