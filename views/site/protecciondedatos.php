<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
use yii\helpers\Html
?>
<body>
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Proteccion de Datos</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">PROTECCIÓN DE DATOS</h1>
    </div>
</div>
    
<!DOCTYPE html>
<html>
<head>
    <title>Nuestro compromiso con la protección de datos personales</title>
</head>
<body>
<h3>NUESTRO COMPROMISO CON LA PROTECCIÓN DE DATOS PERSONALES:</h3>
<h5>"PERSONAS INFORMADAS Y DATOS PROTEGIDOS"</h5>
<br><!-- comment -->
<p>La Dirección / Órgano de Gobierno de Sardinero, HC (en adelante, el responsable del tratamiento), asume la máxima responsabilidad y compromiso con el establecimiento, implementación y mantenimiento de la presente Política de Protección de Datos, garantizando la mejora continua del responsable del tratamiento con el objetivo de alcanzar la excelencia en relación con el cumplimiento del Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos y por el que se deroga la Directiva 95/46/CE (Reglamento general de protección de datos) (DOUE L 119/1, 04-05-2016), y de la normativa española de protección de datos de carácter personal (Ley Orgánica, legislación sectorial específica y sus normas de desarrollo).</p>
<p>La Política de Protección de Datos de Sardinero, HC descansa en el principio de responsabilidad proactiva, según el cual el responsable del tratamiento es responsable del cumplimiento del marco normativo y jurisprudencial que gobierna dicha Política, y es capaz de demostrarlo ante las autoridades de control competentes.</p>
<p>En tal sentido, el responsable del tratamiento se regirá por los siguientes principios que deben servir a todo su personal como guía y marco de referencia en el tratamiento de datos personales:</p>
<ul>
    <li>Protección de datos desde el diseño: el responsable del tratamiento aplicará, tanto en el momento de determinar los medios de tratamiento como en el momento del propio tratamiento, medidas técnicas y organizativas apropiadas, como la seudonimización, concebidas para aplicar de forma efectiva los principios de protección de datos, como la minimización de datos, e integrar las garantías necesarias en el tratamiento.</li>
    <li>Protección de datos por defecto: el responsable del tratamiento aplicará las medidas técnicas y organizativas apropiadas con miras a garantizar que, por defecto, solo sean objeto de tratamiento los datos personales que sean necesarios para cada uno de los fines específicos del tratamiento.</li>
    <li>Protección de datos en el ciclo de vida de la información: las medidas que garanticen la protección de los datos personales serán aplicables durante el ciclo completo de la vida de la información.</li>
    <li>Licitud, lealtad y transparencia: los datos personales serán tratados de manera lícita, leal y transparente en relación con el interesado.</li>
    <li>Limitación de la finalidad: los datos personales serán recogidos con fines determinados, explícitos y legítimos, y no serán tratados ulteriormente de manera incompatible con dichos fines.</li>
    <li>Minimización de datos: los datos personales serán adecuados, pertinentes y limitados a lo necesario en relación con los fines para los que son tratados.</li>
    <li>Exactitud: los datos personales serán exactos y, si fuera necesario, actualizados; se adoptarán todas las medidas razonables para que se supriman o rectifiquen sin dilación los datos personales que sean inexactos con respecto a los fines para los que se tratan.</li>
    <li>Limitación del plazo de conservación: los datos personales serán mantenidos de forma que se permita la identificación de los interesados durante no más tiempo del necesario para los fines del tratamiento de los datos personales.</li>
    <li>Integridad y confidencialidad: los datos personales serán tratados de tal manera que se garantice una seguridad adecuada de los datos personales, incluida la protección contra el tratamiento no autorizado o ilícito y contra su pérdida, destrucción o daño accidental, mediante la aplicación de medidas técnicas u organizativas apropiadas.</li>
    <li>Información y formación: una de las claves para garantizar la protección de los datos personales es la formación e información que se facilite al personal involucrado en el tratamiento de los mismos. Durante el ciclo de vida de la información, todo el personal con acceso a los datos será convenientemente formado e informado acerca de sus obligaciones en relación con el cumplimiento de la normativa de protección de datos.</li>
</ul>
<p>La Política de Protección de Datos de Sardinero, HC es comunicada a todo el personal del responsable del tratamiento y puesta a disposición de todas las partes interesadas.</p>
<p>En su consecuencia, la presente Política de Protección de Datos involucra a todo el personal del responsable del tratamiento, que debe conocerla y asumirla, considerándola como propia, siendo cada miembro responsable de aplicarla y de verificar las normas de protección de datos aplicables a su actividad, así como identificar y aportar las oportunidades de mejora que considere oportunas con el objetivo de alcanzar la excelencia en relación con su cumplimiento.</p>
<p>Esta Política será revisada por la Dirección / Órgano de Gobierno de Sardinero, HC, tantas veces como se considere necesario, para adecuarse, en todo momento, a las disposiciones vigentes en materia de protección de datos de carácter personal.</p>
</body>
</html>