<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Cuotas $model */

$this->title = $model->id_cuotas;
$this->params['breadcrumbs'][] = ['label' => 'Cuotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cuotas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id_cuotas' => $model->id_cuotas], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id_cuotas' => $model->id_cuotas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_cuotas',
            'concepto',
            'metodo_pago',
            'importe',
            'dni',
        ],
    ]) ?>

</div>
