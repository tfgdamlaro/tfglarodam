<?php

use app\models\Cuotas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Cuotas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuotas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Cuotas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'columns' => [
            'id_cuotas',
            'concepto',
            'metodo_pago',
            'importe',
            'dni',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Cuotas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_cuotas' => $model->id_cuotas]);
                 }
            ],
        ],
    ]); ?>


</div>
