<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Cuotas $model */

$this->title = 'Actualizar Cuotas: ' . $model->id_cuotas;
$this->params['breadcrumbs'][] = ['label' => 'Cuotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_cuotas, 'url' => ['view', 'id_cuotas' => $model->id_cuotas]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="cuotas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
