<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Cuotas $model */

$this->title = 'Crear Cuotas';
$this->params['breadcrumbs'][] = ['label' => 'Cuotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuotas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
