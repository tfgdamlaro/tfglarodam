<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Patrocinadores $model */

$this->title = 'Actualizar Patrocinadores: ' . $model->id_patrocinadores;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_patrocinadores, 'url' => ['view', 'id_patrocinadores' => $model->id_patrocinadores]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="patrocinadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
