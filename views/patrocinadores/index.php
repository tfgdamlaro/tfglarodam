<?php

use app\models\Patrocinadores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Patrocinadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Patrocinadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [

            'id_patrocinadores',
            'nombre',
            'dinero',
            'dni',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Patrocinadores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_patrocinadores' => $model->id_patrocinadores]);
                 }
            ],
        ],
    ]); ?>


</div>
