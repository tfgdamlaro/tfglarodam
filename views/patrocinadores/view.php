<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Patrocinadores $model */

$this->title = $model->id_patrocinadores;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="patrocinadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id_patrocinadores' => $model->id_patrocinadores], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id_patrocinadores' => $model->id_patrocinadores], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_patrocinadores',
            'nombre',
            'dinero',
            'dni',
        ],
    ]) ?>

</div>
