<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Preparadores $model */

$this->title = $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Preparadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="preparadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'dni' => $model->dni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'dni' => $model->dni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni',
            'nombre',
            'apellidos',
            [
                'attribute' => 'f_nacimiento',
                'format' => ['Date', 'php:d-m-Y'],
            ],
            [
                'attribute' => 'telefono',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->telefono, 'tel:' . $model->telefono);
                },
            ],
            'sueldo',
            'dni_directivos',
        ],
    ]) ?>

</div>
