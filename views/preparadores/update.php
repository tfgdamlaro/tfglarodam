<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Preparadores $model */

$this->title = 'Actualizar Preparadores: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Preparadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="preparadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
