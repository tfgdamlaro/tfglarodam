<?php

use app\models\Juegan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Convocatorias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Juegan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [

            'id_juegan',
            'dni',
            'id_partidos',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Juegan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_juegan' => $model->id_juegan]);
                 }
            ],
        ],
    ]); ?>


</div>
