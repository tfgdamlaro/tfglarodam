<?php 

use yii\grid\GridView;
use yii\helpers\Html;
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item"><?=Html::a('Partidos',['site/generarpartidos'])?></li>
    <li class="breadcrumb-item active" aria-current="page">Convocatoria</li>
  </ol>
</nav>
<div class="row centrar pb-4">
    <div class="col-md-10">
        <h1 class="text-center titulo">CONVOCATORIA</h1>
    </div>
</div>
<?= Html::a('Eliminar convocatoria', ['juegan/eliminarconvocatoria',
        'id_partidos' => isset($id_partidos) ? $id_partidos : null,
        'codCategoria' => isset($codCategoria) ? $codCategoria : null
    ], ['class' => 'mb-3 float-right btn btn-danger',
         'data' => [
                'confirm' => 'Estas seguro de que quieres eliminar este elemento',
                'method' => 'post']]) ?>
<?php
echo GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $convocados,
        'pagination' => false,
    ]),
    'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
    'summary' => '',
    'emptyText' => 'No hay jugadores convocados. Por favor, crea una convocatoria.',
    'columns' => [
        'nombre',
        'apellidos',
        'dorsal',
    ],
]);
?>

<?= Html::a('Crear convocatoria', ['juegan/generarconvocatorias',
    'id_partidos' => isset($id_partidos) ? $id_partidos : null,
    'codCategoria' => isset($codCategoria) ? $codCategoria : null
    ], ['class' => 'float-left btn btn-success mb-5'])?>
        
<?= Html::a('Actualizar convocatoria', ['juegan/actualizarconvocatorias',
    'id_partidos' => isset($id_partidos) ? $id_partidos : null,
    'codCategoria' => isset($codCategoria) ? $codCategoria : null
    ], ['class' => 'float-right btn btn-primary mb-5']) ?>  




    
