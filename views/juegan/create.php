<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Juegan $model */

$this->title = 'Crear Juegan';
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1,
    ]) ?>

</div>

