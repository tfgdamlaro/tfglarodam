<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Juegan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="juegan-form">

    <?php $form = ActiveForm::begin(); ?>

    
<?= $form->field($model, 'dni')->widget(Select2::classname(), [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 

    
<?= $form->field($model, 'id_partidos')->widget(Select2::classname(), [
            'data' => $data1,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
