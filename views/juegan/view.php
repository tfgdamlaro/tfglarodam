<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Juegan $model */

$this->title = $model->id_juegan;
$this->params['breadcrumbs'][] = ['label' => 'convocatoria', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="juegan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id_juegan' => $model->id_juegan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id_juegan' => $model->id_juegan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_juegan',
            'dni',
            'id_partidos',
        ],
    ]) ?>

</div>
