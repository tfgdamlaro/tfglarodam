<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use app\models\Juegan;
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><?=Html::a('Inicio',['site/index'])?></li>
    <li class="breadcrumb-item"><?=Html::a('Partidos',['site/generarpartidos'])?></li>
    <li class="breadcrumb-item"><?=Html::a('Convocatoria',['juegan/convocados',
        'id_partidos' => $id_partidos,
        'codCategoria' => $codCategoria])?></li>
    <li class="breadcrumb-item active" aria-current="page">Actualizar convocatoria</li>
  </ol>
</nav>
<div class="row centrar">
    <div class="col-md-10">
        <h1 class="text-center titulo">ACTUALIZAR CONVOCATORIA</h1>
    </div>
</div>
<?php
$model = new Juegan();
$form = ActiveForm::begin([
    'action' => ['juegan/insertar-convocatorias']
]);

echo Html::hiddenInput('codCategoria', $codCategoria);
echo $form->field($model, 'id_partidos')->hiddenInput(['value' => $id_partidos])->label(false);

echo GridView::widget([
    'dataProvider' => $jugadores,
    'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
    'summary' => '',
    'layout' => "{items}",
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'jugadoresSeleccionados',
            'checkboxOptions' => function ($model) {
                return ['value' => $model->dni];
            },
        ],
        'nombre',
        'apellidos',
        'dorsal'
    ],
]);

echo Html::submitButton('Guardar', ['class' => 'btn btn-primary']);

ActiveForm::end();?>


