<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Juegan $model */

$this->title = 'Actualizar Convocatorias: ' . $model->id_juegan;
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_juegan, 'url' => ['view', 'id_juegan' => $model->id_juegan]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="juegan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1,
    ]) ?>

</div>
