<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = 'Actualizar Teléfonos: ' . $model->id_telefonos;
$this->params['breadcrumbs'][] = ['label' => 'Teléfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_telefonos, 'url' => ['view', 'id_telefonos' => $model->id_telefonos]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
