<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = $model->id_telefonos;
$this->params['breadcrumbs'][] = ['label' => 'Teléfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="telefonos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id_telefonos' => $model->id_telefonos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id_telefonos' => $model->id_telefonos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_telefonos',
            'dni',
            [
                'attribute' => 'telefonos_tutores_legales',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->telefonos_tutores_legales, 'tel:' . $model->telefonos_tutores_legales);
                },
            ],
        ],
    ]) ?>

</div>
