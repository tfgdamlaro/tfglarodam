<?php

use app\models\Telefonos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Teléfonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Teléfonos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [

            'id_telefonos',
            'dni',
            [
                'attribute' => 'telefonos_tutores_legales',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->telefonos_tutores_legales, 'tel:' . $model->telefonos_tutores_legales);
                },
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Telefonos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_telefonos' => $model->id_telefonos]);
                 }
            ],
        ],
    ]); ?>


</div>
