<?php

use app\models\Jugadores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Jugadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Jugadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [
            'dni',
            'nombre',
            'apellidos',
            [
            'attribute' => 'f_nacimiento',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->f_nacimiento, 'php:d-m-Y');
                },
            ],
            [
                'attribute' => 'telefono',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->telefono, 'tel:' . $model->telefono);
                },
            ],
            //'cod_categoria',
            //'dorsal',
            //'f_alta',
            //'f_baja',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Jugadores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dni' => $model->dni]);
                 }
            ],
        ],
    ]); ?>


</div>
