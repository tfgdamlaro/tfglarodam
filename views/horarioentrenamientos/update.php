<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Horarioentrenamientos $model */

$this->title = 'Actualizar Horarios entrenamientos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Horarioentrenamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="horarioentrenamientos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1
    ]) ?>

</div>
