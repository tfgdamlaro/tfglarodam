<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Horarioentrenamientos $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Horarioentrenamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="horarioentrenamientos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'diasemana',
            [
                'attribute' => 'hinicio',
                'format' => ['time', 'php:H:i'],
            ],
            [
                'attribute' => 'hfinal',
                'format' => ['time', 'php:H:i'],
            ],
            'cod_categoria',
        ],
    ]) ?>

</div>
