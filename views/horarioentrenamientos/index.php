<?php

use app\models\Horarioentrenamientos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Horarioentrenamientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horarioentrenamientos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Horarios entrenamientos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [


            'id',
            'diasemana',
            [
            'attribute' => 'hinicio',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->hinicio, 'php: H:i');
                },
            ],
            [
            'attribute' => 'hfinal',
                'value' => function ($model) {
                    return Yii::$app->formatter->asTime($model->hfinal, 'php: H:i');
                },
            ],
            'cod_categoria',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Horarioentrenamientos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
