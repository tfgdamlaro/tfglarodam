<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\select2\Select2;

/** @var yii\web\View $this */
/** @var app\models\Horarioentrenamientos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="horarioentrenamientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'diasemana')->widget(Select2::classname(), [
            'data' => $data1,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 

    <?= $form->field($model, 'hinicio')->widget(TimePicker::classname(), [
        'containerOptions' => ['style' => 'width: fit-content;'],
        'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 5,
        ],
    ]);
    ?>

    <?= $form->field($model, 'hfinal')->widget(TimePicker::classname(), [
        'containerOptions' => ['style' => 'width: fit-content;'],
        'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 5,
        ],
    ]);
    ?>

    <?= $form->field($model, 'cod_categoria')->widget(Select2::classname(), [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
