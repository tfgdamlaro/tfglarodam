<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Horarioentrenamientos $model */

$this->title = 'Crear Horarioentrenamientos';
$this->params['breadcrumbs'][] = ['label' => 'Horarioentrenamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horarioentrenamientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1
    ]) ?>

</div>
