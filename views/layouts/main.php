<?php
/** @var yii\web\View $this */

/** @var string $content */
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <!-- on your view layout file HEAD section -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>
    <body class="d-flex flex-column h-100">
        <?php $this->beginBody() ?>

        <header>
            <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/img/LogoSardinero.png', ['alt' => Yii::$app->name]),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-expand-md navbar-custom fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    ['label' => 'HOME', 'url' => ['/site/index']],
                    ['label' => 'NUESTRO CLUB', 'url' => ['/site/about']],
                    ['label' => 'CONTACTO', 'url' => ['/site/contact']],
                    ['label' => 'CATEGORÍAS', 'url' => ['/site/infocategoria']],
                    ['label' => 'PROGRAMACIÓN', 'url' => ['/site/mostrarentrenamientos']],
                    ['label' => 'ESTADÍSTICAS', 'url' => ['/site/estadisticascategorias']],
                    Yii::$app->user->isGuest ? (
                            ['label' => 'Login', 'url' => ['/site/login']]
                            ) : (
                            '<li>'
                            . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                            . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->username . ')',
                                    ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>'
                            )
                ],
            ]);

            echo '<ul class="navbar-nav ml-auto">';
            echo '<li class="nav-item">';
            echo '<a href="https://www.facebook.com/people/Sardinero-HC/100057693848375/" class="icono p-3"><i class="fab fa-facebook-square"></i></a>';
            echo '</li>';
            echo '<li class="nav-item">';
            echo '<a href="https://twitter.com/sardinerohc" class="icono"><i class="fab fa-twitter-square"></i></a>';
            echo '</li>';
            echo '<li class="nav-item">';
            echo '<a href="https://www.instagram.com/sardinerohcmasc/" class="icono p-3"><i class="bi bi-instagram"></i></a>';
            echo '</li>';
            echo '</ul>';

            NavBar::end();

            ?>
            
        </header>
        <?php if (!Yii::$app->user->isGuest){ ?>
            <aside class="miDiv">
                <div class="pt-5 mt-3 pr-4 pl-3">
                    <?php
                        echo Nav::widget([
                            'options' => ['class' => 'navbar-nav ml-auto'],
                            'items' => [
                                ['label' => 'Eventos', 'url' => ['/site/mostrarentrenamientos']],
                                ['label' => 'Ver Convocatorias', 'url' => ['/site/generarpartidos']],
                                ['label' => 'Información', 'url' => ['/site/infocategoria']],
                                ['label' => 'Programación', 'url' => ['/site/mostrarentrenamientos']],
                                [
                                    'label' => 'ESTADISTICAS',
                                    'items' => [
                                        ['label' => 'Estadisticas capital', 'url' => ['/site/estadisticas']],
                                        ['label' => 'Estadisticas categorias', 'url' => ['/site/estadisticascategorias']],
                                    ],
                                ],    
                                [
                                    'label' => 'CRUDS',
                                    'items' => [
                                        ['label' => 'Categoria', 'url' => ['/categoria/index']],
                                        ['label' => 'Entrenadores', 'url' => ['/entrenadores/index']],
                                        ['label' => 'Cuotas', 'url' => ['/cuotas/index']],
                                        ['label' => 'Directivos', 'url' => ['/directivos/index']],
                                        ['label' => 'Convocatorias', 'url' => ['/juegan/index']],
                                        ['label' => 'Jugadores', 'url' => ['/jugadores/index']],
                                        ['label' => 'Partidos', 'url' => ['/partidos/index']],
                                        ['label' => 'Patrocinadores', 'url' => ['/patrocinadores/index']],
                                        ['label' => 'Preparadores', 'url' => ['/preparadores/index']],
                                        ['label' => 'Teléfonos tutores legales', 'url' => ['/telefonos/index']],
                                        ['label' => 'Horarios', 'url' => ['/horarioentrenamientos/index']],
                                        ['label' => 'Noticias', 'url' => ['/noticias/index']],
                                    ],
                                ],    
                            ]
                        ]);
                    ?>
                </div>
            </aside>
        <?php } ?>
        <main role="main" class="flex-shrink-0">
            <div class="container">
<?=
Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])
?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </main>

        <footer class="footer mt-auto py-3 text-muted footer-custom h-auto ">
            <div class="container">
                <div class="row">
                    <div class="col-4 d-flex flex-column justify-content-center align-items-center">
                        <?= Html::img('@web/img/Dromedario.png', ['alt' => Yii::$app->name]) ?>
                        <?= Html::img('@web/img/Solares.png', ['alt' => Yii::$app->name]) ?>
                        <?= Html::img('@web/img/camninolebaniego2.png', ['alt' => Yii::$app->name]) ?>
                    </div>
                    <div class="col-4 d-flex flex-column justify-content-center align-items-center">
                        <div>
                            <a href="https://www.facebook.com/people/Sardinero-HC/100057693848375/" class="icono p-3"><i class="fab fa-facebook-square"></i></a>
                            <a href="https://twitter.com/sardinerohc" class="icono"><i class="fab fa-twitter-square"></i></a>
                            <a href="https://www.instagram.com/sardinerohcmasc/" class="icono p-3"><i class="bi bi-instagram"></i></a>
                        </div>
                        <div class="btn-group-vertical " role="group">
                            <?= Html::a('Contacto', ['/site/contact'],['class' => 'btn custom-button text-secondary']) ?>
                            <?= Html::a('Condiciones Legales', ['/site/condicioneslegales'],['class' => 'btn custom-button text-secondary']) ?>
                            <?= Html::a('Politica de Privacidad', ['/site/politicadeprivacidad'],['class' => 'btn custom-button text-secondary']) ?>
                            <?= Html::a('Proteccion de Datos', ['/site/protecciondedatos'],['class' => 'btn custom-button text-secondary']) ?>
                        </div>
                    </div>
                    <div class="col-4 d-flex flex-column justify-content-center align-items-center">
                        <?= Html::img('@web/img/imd.png', ['alt' => Yii::$app->name]) ?>
                        <?= Html::img('@web/img/logo_gobierno-300x147-1-300x147.png', ['alt' => Yii::$app->name]) ?>
                        <?= Html::img('@web/img/logo_ZonaFranca-300x143.png', ['alt' => Yii::$app->name]) ?>
                    </div>
                </div>
              </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
