<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Directivos $model */

$this->title = 'Actualizar Directivos: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Directivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="directivos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
