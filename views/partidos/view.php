<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Partidos $model */

$this->title = $model->id_partidos;
$this->params['breadcrumbs'][] = ['label' => 'Partidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="partidos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id_partidos' => $model->id_partidos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id_partidos' => $model->id_partidos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_partidos',
            'resultadoS',
            'resultadoC',
            [
                'attribute' => 'fecha',
                'format' => ['Date', 'php:d-m-Y'],
            ],
            [
                'attribute' => 'hora',
                'format' => ['time', 'php:H:i'],
            ],
            'competicion',
            'contrincante',
            'cod_categoria',
        ],
    ]) ?>

</div>
