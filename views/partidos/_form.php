<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\date\DatePicker;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Partidos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="partidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'competicion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contrincante')->textInput(['maxlength' => true]) ?>

    
<?= $form->field($model, 'cod_categoria')->widget(Select2::classname(), [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?>

    <?= $form->field($model, 'resultadoS')->textInput() ?>

    <?= $form->field($model, 'resultadoC')->textInput() ?>

    <?= $form->field($model, 'hora')->widget(TimePicker::classname(), [
        'containerOptions' => ['style' => 'width: fit-content;'],
        'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 5,
        ],
    ]);
    ?>

    <?= $form->field($model, 'fecha')->widget(DatePicker::classname(), [
            'language' => 'es',
            'type' => DatePicker::TYPE_INPUT,
            'value' => '23-Feb-1982',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy/mm/dd'
            ]
            
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
