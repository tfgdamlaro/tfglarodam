<?php

use app\models\Partidos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Partidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [

            'id_partidos',
            'resultadoS',
            'resultadoC',
            [
            'attribute' => 'fecha',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->fecha, 'php:d-m-Y');
                },
            ],
            [
            'attribute' => 'hora',
                'value' => function ($model) {
                    return Yii::$app->formatter->asTime($model->hora, 'php:H:i');
                },
            ],
            //'competicion',
            //'contrincante',
            //'cod_categoria',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Partidos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_partidos' => $model->id_partidos]);
                 }
            ],
        ],
    ]); ?>


</div>
