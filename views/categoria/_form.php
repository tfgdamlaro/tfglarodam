<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Categoria $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="categoria-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_categoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dni')->widget(Select2::classname(), [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
