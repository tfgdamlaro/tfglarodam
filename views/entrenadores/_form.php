<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\widgets\Select2;

/** @var yii\web\View $this */
/** @var app\models\Entrenadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="entrenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'f_nacimiento')->widget(DatePicker::classname(), [
            'language' => 'es',
            'type' => DatePicker::TYPE_INPUT,
            'value' => '23-Feb-1982',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy/mm/dd'
            ]
            
        ]);
    ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sueldo')->textInput() ?>

    <?= $form->field($model, 'cod_categoria')->widget(Select2::classname(), [
            'data' => $data1,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 

    <?= $form->field($model, 'dni_directivos')->widget(Select2::classname(), [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?> 

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
