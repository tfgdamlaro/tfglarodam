<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Entrenadores $model */

$this->title = 'Actualizar Entrenadores: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="entrenadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1,
    ]) ?>

</div>
