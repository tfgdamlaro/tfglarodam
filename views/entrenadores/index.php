<?php

use app\models\Entrenadores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Entrenadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-hover my-gridview '],
        'summary' => '',
        'filterModel' => $searchModel,
        'columns' => [

            'dni',
            'nombre',
            'apellidos',
            [
            'attribute' => 'f_nacimiento',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->f_nacimiento, 'php:d-m-Y');
                },
            ],
            [
                'attribute' => 'telefono',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->telefono, 'tel:' . $model->telefono);
                },
            ],
            //'sueldo',
            //'cod_categoria',
            //'dni_directivos',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Entrenadores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dni' => $model->dni]);
                 }
            ],
        ],
    ]); ?>


</div>
